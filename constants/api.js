import axios from 'axios';
// const URL = 'http://3.109.218.3:8080';
const URL = 'http://localhost:8080';
export const API_URL = URL + '/api';



const axiosClient = axios.create({ baseURL: API_URL });
axiosClient.interceptors.request.use((config) => {
  if (localStorage.getItem('token')) {
    config.headers.Authorization = `${localStorage.getItem(
      'token'
    )}`;
  } else {
    config.headers.Authorization = `${sessionStorage.getItem(
      'token'
    )}`;
  }
  return config;
});

axiosClient.interceptors.response.use(
  (res) => {
    if (res.status === 200 || res.status === 201) {
      return res.data;
    } else {
      throw new Error('Network response error');
    }
  },
  (err) => {
    // let growl = store?.getState()?.appReducer.growl;
    console.log(err.response);
    console.log(err);

    // growl.show({ severity: 'error', life: '10000', summary: err.response.data.message })
    // toast.warning('Danger')
    // growl.show({ severity: 'error', life: '10000', summary: err.message });

    if (err.response) {
      // 4XX or 5XX
      // if(err.response.data.code = 401)
      // {
      // growl.show({ severity: 'error', life: '10000', summary: err.response.data.message });
      // }
      console.log(err.response);
      if (err.response.data.code === 500) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message === 'Wrong previous password'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message ===
          'Sheet is empty. No data has been imported.'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message ===
          'This user is used as notification Receiver.'
      ) {
        toast.error('This user is used as notification Receiver.', {
          autoClose: 5000
        });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message === 'This asset is associated with Amc.'
      ) {
        toast.error('This asset is associated with Amc.', { autoClose: 5000 });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message === 'The vendor is associated with Amc.'
      ) {
        toast.error('The vendor is associated with Amc', { autoClose: 5000 });
      } else if (
        err.response.data.code === 409 &&
        err.response.data.message === 'Email id already registered'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if ((err.response.data.code = 409)) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 401 &&
        err.response.data.message === 'Serial number already exists'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 401 &&
        err.response.data.message === 'Email Id already registered'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 401 &&
        err.response.data.message === 'GSTIN already exists'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 401 &&
        err.response.data.message === 'Model number already exists'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (
        err.response.data.code === 401 &&
        err.response.data.message ===
          'Serial number and Model number already exist'
      ) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else if (err.response.status === 401) {
        toast.error(err.response.data.message, { autoClose: 5000 });
      } else {
        toast.error('server not found', { autoClose: 5000 });
      }
      console.log(err);
    } else {
      console.log(err);
      // if(!err.response)
      // {
      toast.error('server not found', { autoClose: 5000 });
      return;
      // }
      // toast.error("server not found", { autoClose: 5000 });
    }
    throw new Error(err);
  }
);

export function userId() {
  if (localStorage.getItem('uuId')) {
    return JSON.parse(localStorage["uuId"]);
  } else if (sessionStorage.getItem('uuId')) {
    return JSON.parse(sessionStorage.getItem('uuId'));
  } else {
    localStorage.clear();
    sessionStorage.clear();
    <BrowserRouter>
      <Switch>
        <Redirect to='/login' />
      </Switch>
    </BrowserRouter>;
  }
}

const addUserId = (url) => {
  return url.replace(/#userId/gi, () => {
    console.log(userId());
    return userId();
  });
};

const get = (url) => {
  console.log(url);
  return axiosClient.get(addUserId(url));
};

const post = (url, data) => {
  console.log(url);
  return axiosClient.post(addUserId(url), data);
};

const forgotPasword = (url,data) => {
 data = JSON.stringify(data)
  return axiosClient.post(`${url}?email=${data}`);
};

const put = (url, data) => {
  return axiosClient.put(addUserId(url), data);
};

const remove = (url) => {
  return axiosClient.delete(addUserId(url));
};

const upload = (url, data) => {
  console.log(url);
  return axiosClient.post(addUserId(url), data);
};

const httpClient = { get, post, put, remove, upload, forgotPasword, };
export { httpClient };


