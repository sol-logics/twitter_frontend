import { httpClient } from "./api";

function getUserdata() {
  return httpClient.get("/users/#userId/profiledetail");
}

function onUploadAmc(data) {
  return httpClient.post('/users/#userId/amc/upload', data);
}

function onUploadAmcConfirmation(data) {
  return httpClient.put('/users/#userId/amc/upload', data);
}
function addAmc(amcData)
{
  console.log("this is post amc Data",amcData)
  return httpClient.post('/users/#userId/amc', amcData)
}
function UploadAmcData(amcData)
{
  console.log("this is post amc Data",amcData)
  return httpClient.upload('/users/#userId/amc', amcData)
}
function upLoadAmcAttachemnt(file)
{
  console.log("this is post amc Data",file)
  return httpClient.post('/users/#userId/uploadFile', file)
}



function get_AmcOnId(renewUuid) {  
  return httpClient.get(`users/#userId/amc/${renewUuid}`);
}




function get_AmcOnIdRenew(renewUuid) {  
  return httpClient.get(`users/#userId/renewamc/${renewUuid}`);

}

function deleteAmcOnId(amcId) {
  return httpClient.remove(`users/#userId/amc/${amcId}`);
}

function updateAmcOnId(amcId, amcData) {
  return httpClient.put(`/users/#userId/amc/${amcId}`, amcData);
}

function getStatus()
{
  return httpClient.get(`users/#userId/status`);
}







export { getUserdata, upLoadAmcAttachemnt, onUploadAmcConfirmation, onUploadAmc,
  
  get_AmcOnIdRenew, UploadAmcData,addAmc, updateAmcOnId, get_AmcOnId, deleteAmcOnId, getStatus };
