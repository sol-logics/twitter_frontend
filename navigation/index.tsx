/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
 import { FontAwesome } from '@expo/vector-icons';
 import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
 import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
 import { createNativeStackNavigator } from '@react-navigation/native-stack';
 import * as React from 'react';
 import { Image, ColorSchemeName, Pressable, StyleSheet } from 'react-native';
 
 import Colors from '../constants/Colors';
 import useColorScheme from '../hooks/useColorScheme';
 import ModalScreen from '../screens/ModalScreen';
 import NotFoundScreen from '../screens/NotFoundScreen'; 
 import Signup from '../screens/Signup';
 import VerifyUser from '../screens/VerifyUser';
 import WelcomeSignUp from '../screens/WelcomeSignUp';
 import Login from '../screens/Login';
 import Home from '../screens/Home';
 import Profile from '../screens/UserProfile';
 import TweetPostDetailsDataCard from '../screens/TweetPostDetailsDataCard';
 import ReplyTweet from '../screens/ReplyTweet';
 import PostTweet from '../screens/PostTweet';
 import TweetCommentDataCard from '../screens/TweetCommentDataCard';
 import ReplyComment from '../screens/ReplyComment';
 import Search from '../screens/Search'; 
 import Notifications from '../screens/Notifications';
 import Following from '../screens/FollowingList';
 import Follower from '../screens/FollowerList'; 
 import FollowerRequests from '../screens/FollowerRequests';
 import VerifyOtpForFogotPassword from '../screens/VerifyOtpForFogotPassword';
 import SetNewPassword from '../screens/SetNewPassword';
 import SetNewPasswordSuccess from '../screens/SetNewPasswordSuccess'; 
 import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
 import LinkingConfiguration from './LinkingConfiguration';
 import { Text, View } from '../components/Themed'; 

 import userImageUrl from "../assets/images/user.jpg";
 import GetOtpForFogotPassword from './../screens/GetOtpForFogotPassword';
import ChangePassword from '../screens/changePassword';

 export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {

  function logoutUser(navigation) {
      localStorage.clear();
      navigation.push("LoginTab");
  }


   return (     
     <NavigationContainer
       linking={LinkingConfiguration}
       theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>      
       <RootNavigator />
     </NavigationContainer>
     
   );
 }
 
 /**
  * A root stack navigator is often used for displaying modals on top of all other content.
  * https://reactnavigation.org/docs/modal
  */
 const Stack = createNativeStackNavigator<RootStackParamList>();
 
 export function RootNavigator() {
   return (
    <Stack.Navigator screenOptions={{ headerShown: false}} >
       <Stack.Screen name="Root" component={Login} options={{ headerShown: false }} initial = {true} />
       <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
       <Stack.Screen name="LoginTab" component={Login} options={ { headerShown: false}} />
       <Stack.Screen name="SignupTab" component={Signup} options={{headerShown: false}} />
       <Stack.Screen name="VerifyUserTab" component={VerifyUser} options={{headerShown: false}} />
       <Stack.Screen name="WelcomeSignUpTab" component={WelcomeSignUp} options={{headerShown: false}} />
       <Stack.Screen name="GetOtpForFogotPasswordTab" component={GetOtpForFogotPassword} options={{headerShown: false}} />
       <Stack.Screen name="VerifyOtpForFogotPasswordTab" component={VerifyOtpForFogotPassword} options={{headerShown: false}} />
       <Stack.Screen name="SetNewPasswordTab" component={SetNewPassword} options={{headerShown: false}} />
       <Stack.Screen name="SetNewPasswordSuccessTab" component={SetNewPasswordSuccess} options={{headerShown: false}} />       
       <Stack.Screen name="PostTweetTab" component={PostTweet} options={{headerShown: false}} />
       <Stack.Screen name="ReplyTweetTab" component={ReplyTweet} options={{headerShown: false}} />
       <Stack.Screen name="TweetCommentTab" component={TweetCommentDataCard} options={{headerShown: false}} />
       <Stack.Screen name="ReplyCommentTab" component={ReplyComment} options={{headerShown: false}} />

       

       <Stack.Screen name="Home" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="Profile" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="Notifications" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="FollowerRequests" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="Following" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="Follower" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="ChangePassword" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="Search" component={BottomTabNavigator} options={ {headerShown: false}} />
       <Stack.Screen name="TweetPostDetailsDataCardTab" component={BottomTabNavigator} options={{headerShown: false}} />

     </Stack.Navigator>

      
   );
 }
 
 /**
  * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
  * https://reactnavigation.org/docs/bottom-tab-navigator
  */
 const BottomTab = createBottomTabNavigator<RootTabParamList>();
 
function BottomTabNavigator() {
   const colorScheme = useColorScheme();
 
   return (
     <BottomTab.Navigator
       screenOptions={{ tabBarActiveTintColor: Colors[colorScheme].tint}}>       
       <BottomTab.Screen name="Home" component={Home} options={{ headerShown: false, tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,}} />
       <BottomTab.Screen name="Search" component={Search} options={{headerShown: false, tabBarIcon: ({ color }) => <TabBarIcon name="search" color={color} />,}} />
       <BottomTab.Screen name="Notifications" component={Notifications} options={{headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
       <BottomTab.Screen name="Profile" component={Profile} options={{tabBarButton: (props) => null, headerShown: false, tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,}} />
       <BottomTab.Screen name="TweetPostDetailsDataCardTab" component={TweetPostDetailsDataCard} options={{tabBarButton: (props) => null, headerShown: false, tabBarIcon: ({ color }) => <TabBarIcon name="search" color={color} />,}} />
       <BottomTab.Screen name="FollowerRequests" component={FollowerRequests} options={{tabBarButton: (props) => null, headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
       <BottomTab.Screen name="Following" component={Following} options={{tabBarButton: (props) => null, headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
       <BottomTab.Screen name="Follower" component={Follower} options={{tabBarButton: (props) => null, headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
       <BottomTab.Screen name="ReplyTweetTab" component={ReplyTweet} options={{tabBarButton: (props) => null, headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
       <BottomTab.Screen name="ChangePassword" component={ChangePassword} options={{tabBarButton: (props) => null, headerShown: false,  tabBarIcon: ({ color }) => <TabBarIcon name="bell" color={color} />,}} />
   </BottomTab.Navigator>
   );
 }
 
 /**
  * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
  */
 function TabBarIcon(props: {
   name: React.ComponentProps<typeof FontAwesome>['name'];
   color: string;
 }) {
   return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
 }



 const styles = StyleSheet.create({  
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  }
});
 