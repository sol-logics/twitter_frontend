import * as WebBrowser from 'expo-web-browser';
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import Colors from '../constants/Colors';
import { Text, View } from '../components/Themed';

import * as $ from 'jquery';
import logoUrl from "../assets/images/logo.png";
import Navigation from './../navigation/index';
import { API_URL } from '../constants/api';

export default function VerifyUser({navigation}) {

  const [errorMsg, setErrorMsg] = useState("");
  const [statusMsg, setStatusMsg] = useState("");
  const [oneTimePassword, setOneTimePassword] = useState("");
  const [emailId, setEmailId] = useState(localStorage["userEmailForSignup"]);

  function verifyUserOTP(emailId, oneTimePassword) {

    try{
      // var postUrl   = "http://localhost:8080/api/otpVerification";
      var postUrl   = `${API_URL}/otpVerification`;
      var tmpData = {"email": emailId, "otp": oneTimePassword};
      console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({							
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpData),
            dataType: "json",
            beforeSend: function (xhr) {
                //Send Authorization token after user logged in
            },
            success : function(result) {
                response = result;
                console.log("response -- " + JSON.stringify(response));
                if(response.status >= 200 && response.status <= 300){     
                  setErrorMsg("");
                  navigation.push("WelcomeSignUpTab");
                }else{                  
                  setErrorMsg(response.message);
                }
        },
        error : function(xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
        }
        });				
      }catch(e){
          alert(e);
      }
  }

  function resendUserOTP(emailId, navigation) {

    try{
      // var postUrl   = "http://localhost:8080/api/resendOtp";
      var postUrl  = `${API_URL}/resendOtp`
      var tmpData = {"email": emailId};
      console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({							
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpData),
            dataType: "json",
            beforeSend: function (xhr) {
                //Send Authorization token after user logged in
            },
            success : function(result) {
                response = result;                
                if(response.status >= 200 && response.status <= 300){
                  setStatusMsg("OTP has been resent successfully");
                }else{                  
                  setErrorMsg(response.message);
                }
        },
        error : function(xhr) {
            console.log(xhr.responseJSON.message);
            setErrorMsg(xhr.responseJSON.message);
        }
        });				
      }catch(e){
          alert(e);
      }    
  }


  return (
    <View style={styles.container}>

       <View style={styles.headerContainer}>            
        <img src={logoUrl} style={{ width: 70, margin: '0px auto', float: 'left'}} alt="Logo"   />
       </View>
      
      <Text style={styles.title}>Please enter OTP</Text>
      
      <Text style={styles.errorMsgCls}>{errorMsg}</Text>
      <Text style={styles.statusMsgCls}>{statusMsg}</Text>

      <TextInput name="verifyOneTimePassword" style={styles.textInputField} placeholder="OTP" maxLength={6} value={oneTimePassword} onChange={(e) => setOneTimePassword(e.target.value)} />

      <TouchableOpacity  onPress={() => resendUserOTP(emailId, navigation)}style={styles.helpLink}>
              <Text style={styles.helpLinkText} lightColor={Colors.light.tint}>Resend OTP</Text>
      </TouchableOpacity>

      <View style={styles.helpContainer}>   

            <TouchableOpacity onPress={() => verifyUserOTP(emailId, oneTimePassword)} style={styles.signInActionButton}>
              <Text style={{ fontSize: 17, color: '#fff', textAlign: 'center'}}>Verify User</Text>
            </TouchableOpacity>

        <TouchableOpacity  onPress={() => navigation.push("LoginTab")}style={styles.helpLink}>
          <Text style={styles.helpLinkText} lightColor={Colors.light.tint}>
            Back to Login?
          </Text>
        </TouchableOpacity>

      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: '20px',
    width: '80%'
  },
  errorMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  errorMsgCls: {
    fontSize: 15,
    color: 'red',
    textAlign: 'left',
    marginLeft: '20px',
    marginBottom: '10px',
  },
  statusMsgCls: {
    fontSize: 15,
    color: 'green',
    textAlign: 'left',
    marginLeft: '20px',
    marginBottom: '10px',
  },
  statusMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  statusMsgCls: {
    fontSize: 15,
    color: 'green',
    marginTop: '15px'
  },
  signinLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  textInputField: {
    width: '81%',
    padding: '10px',
    textAlign: 'left',
    marginTop: '10px',
    borderStyle: 'solid',
    borderColor: '#ccc', 
    borderWidth: 1
  },
  helpContainer: {    
    alignItems: 'center',
    marginTop: '233px'
  },
  actionButtonContainer: {
    flexDirection: 'row'
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
  signUpContainer: {
    flexDirection: 'row'
  },
  signUpLinkText: {
    textAlign: 'center',
    fontSize: 15
  },
  signInActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '151px', 
    paddingTop: '7px', 
    marginRight: '20px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  },
  signUpActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '151px', 
    paddingTop: '7px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  }


});
