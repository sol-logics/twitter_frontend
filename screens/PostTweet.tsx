import * as WebBrowser from "expo-web-browser";
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";
import Moment from 'moment';
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import addTweetImageUrl from "../assets/images/add_tweet_btn.png";
import { API_URL } from "../constants/api";

export default function TweetCommentDataCard(props) {

  var navigation = props.navigation;

  const [tweetContent, setTweetContent] = useState("");
  const [tweetStatusMsg, setTweetStatusMsg] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  const [showTweetView, setShowTweetView] = useState("false");
  console.log("showTweetView -- " + showTweetView);

  function postTweet(navigation, tweetContent){
    try{       
      var userUUID  = localStorage["uuId"];
      var authToken = localStorage["token"];
      console.log("tweetContent -- " + tweetContent);
      // var postUrl    = "http://localhost:8080/api/users/"+userUUID+"/tweets";
      var postUrl    = `${API_URL}/users/${userUUID}/tweets`;
      var tmpDataNew = {"content": tweetContent, "status": 'Enabled'};
      console.log("tmpDataNew -- " + JSON.stringify(tmpDataNew));
      var response = null;
      $.ajax({
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpDataNew),
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("x-access-token", authToken);
            },
            success : function(result) {
                response = result;                
                if(response.status >= 200 && response.status <= 300){
                    // navigation.push("Home");
                    navigation.push("Home");

                    setTweetStatusMsg("Your Tweet was sent");
                    setShowTweetView("false");
                }else{
                    setErrorMsg(response.message);
                }
            },
            error : function(xhr) {
                setErrorMsg(xhr.responseJSON.message);
            }
        });				
      }catch(e){
          alert(e);
      }
  }
  
  function afterClick()
  {
    tweetContent.trim()
  }


 return (
    <View style={styles.container}>
            <View style={styles.addPostTweetBtnContainer}>
                <textarea name="tweetContent" style={{padding: '5px', width: 'auto', margin:"3px", height: '100px', fontSize: 15, color: "#000", textAlign: "left" }} placeholder="What’s happening" value={tweetContent} onChange={(e) => setTweetContent(e.target.value)}></textarea>
                <View style={styles.createPostTweetBtnContainer}>
                      <TouchableOpacity onPress={() => navigation.push("Home")} style={styles.actionButton}>
                          <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Close</Text>
                      </TouchableOpacity>

                      <TouchableOpacity 
                          disabled={tweetContent == ""}
                      onPress={() => postTweet(navigation, tweetContent)} style={styles.actionButton}>
                          <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Tweet</Text>
                      </TouchableOpacity>
                </View>            
            </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '4%',
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },  
  showPostTweetBtnContainer: {
    width: '98%',
  },
  addPostTweetBtnContainer: {
    width: '98%',
  },
  createPostTweetBtnContainer: {
        width: '100%',
        flexDirection: "row",
        // marginLeft: '54%',
  },
  subMiddleContainer: { 
    flexDirection: "row",
    marginLeft: '20px',
    marginTop: '20px',
    width: "98%",
  },  
  textInputField: {
    width: "96%",
    height: "100px",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  actionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "100px",
    paddingTop: "5px",
    paddingBottom: "10px",
    borderRadius: 9999,
    marginTop: '20px',
    marginRight: '20px', 
  },
  title: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: '3px',
    width: "70%",
  },
  tweetContent: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",    
  },
  linkText: {
    flexDirection: "row"
  },
});
