import * as WebBrowser from "expo-web-browser";
import { useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import * as $ from "jquery";

import logoUrl from "../assets/images/logo.png";
import { API_URL } from "../constants/api";

export default function Signup({ navigation }) {
  const [errorMsg, setErrorMsg] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function signUpNewUser(navigation, email, password) {
    var isUserExists = checkUserExists();
    if (isUserExists == "true") {
      setErrorMsg("User is already exists");
      console.log("User is exists");
    }
    if (isUserExists == "false") {
      saveValueIntoDBAndSendOTPToEmail(navigation, email, password);
    }
  }

  function checkUserExists() {
    //Check email is exists or not if user exists then show message user exists otherwise save email and password into database
    return "false";
  }

  function saveValueIntoDBAndSendOTPToEmail(navigation, email, password) {
    try {
      var postUrl = `${API_URL}/signup`;
      // var postUrl   = "http://localhost:8080/api/signup";
      var tmpDataNew = { email: email, password: password };
      console.log("tmpDataNew -- " + JSON.stringify(tmpDataNew));
      var response = null;
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: postUrl,
        data: JSON.stringify(tmpDataNew),
        dataType: "json",
        beforeSend: function (xhr) {
          //Send Authorization token after user logged in
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
            setErrorMsg("");
            localStorage["userEmailForSignup"] = email;
            console.log(
              "userEmailForSignup " + localStorage["userEmailForSignup"]
            );
            navigation.push("VerifyUserTab");
          } else {
            setErrorMsg(response.message);
          }
        },
        error: function (xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <img
          src={logoUrl}
          style={{ width: 70, margin: "0px auto", float: "left" }}
          alt="Logo"
        />
      </View>

      <Text style={styles.title}>
        To get started, first enter your email, password and confirm password
      </Text>

      <Text style={styles.errorMsgCls}>{errorMsg}</Text>

      <TextInput
        name="email"
        id="email"
        style={styles.textInputField}
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextInput
        name="password"
        id="password"
        secureTextEntry={true}
        style={styles.textInputField}
        placeholder="New password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <TextInput
        name="confirmPassword"
        secureTextEntry={true}
        style={styles.textInputField}
        placeholder="Confirm new password"
      />

      <View style={styles.helpContainer}>
        <TouchableOpacity
          disabled={email == "" || password == ""}
          onPress={() => signUpNewUser(navigation, email, password)}
          style={styles.signInActionButton}
        >
          <Text style={{ fontSize: 17, color: "#fff", textAlign: "center" }}>
            Next
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.push("LoginTab")}
          style={styles.helpLink}
        >
          <Text style={styles.helpLinkText} lightColor={Colors.light.tint}>
            Back to Login?
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: "20px",
    marginLeft: "34px",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    margin: "20px",
    width: "80%",
  },
  errorMsgCls: {
    fontSize: 15,
    color: "red",
    textAlign: "left",
    marginBottom: "10px",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "150px",
  },
  errorMsgContainer: {
    alignItems: "center",
    marginTop: "50px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "7px",
    marginRight: "20px",
    height: "40px",
    borderRadius: 9999,
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "7px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
});
