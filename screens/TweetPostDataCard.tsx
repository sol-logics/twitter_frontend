import * as WebBrowser from "expo-web-browser";
import { useEffect, useState } from "react";
import { Snackbar } from "react-native-paper";
import {
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Moment from "moment";
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from "jquery";
import TweetCommentDataCard from "../screens/TweetCommentDataCard";
import userImageUrl from "../assets/images/user.jpg";
import likeImageUrl from "../assets/images/likes.png";
import likedImageUrl from "../assets/images/liked.png";
import optionImage from "../assets/images/options.png";
import retweetImage from "../assets/images/retweet.png";
import replyToImageUrl from "../assets/images/twitter_reply.png";
import { Button, Paragraph, Dialog, Portal } from "react-native-paper";
import { API_URL } from "../constants/api";

export default function TweetPostDataCard(props) {
  Moment.locale("en");
  var postDate = props.tweetPostRow.createdDate;
  var likeFlag = props.tweetPostRow.likeFlag;

  console.log(props)

  var userLikeImageUrl = likeImageUrl;
  var navigation = props.navigationData;

  if (likeFlag == "true") {
    userLikeImageUrl = likedImageUrl;
  }

  const [visible, setVisible] = useState(false);
  const onToggleSnackBar = () => setVisible(!visible);
  const onDismissSnackBar = () => setVisible(false);

  // console.log(props.navigationData);

  var tweetUserName = localStorage["name"];
  const [tweetCommentList, setTweetCommentList] = useState([]);
  const [tweetLikesCount, setTweetLikesCount] = useState(0);
  const [errorMsg, setErrorMsg] = useState("");
  const [tweetReply, setTweetReply] = useState("");

  function showTweetReplyPage(tweetId) {
    navigation.push("ReplyTweetTab", { tweetId });
  }

  function navigateOnTweetDetailsPage(tweetId) {
    navigation.navigate("TweetPostDetailsDataCardTab", { tweetId });
  }

  function likePost(tweetId, likeFlag) {
    var requestMethod = "POST";
    if (likeFlag == "true") {
      requestMethod = "Delete";
    }

    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID +"/tweets/" +tweetId +"/like";
      var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}/like`;
      var response = null;
      $.ajax({
        type: requestMethod,
        url: postUrl,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
            //console.log("Tweet Like response -- " + JSON.stringify(response));
            navigation.push("Home");
          }
        },
        error: function (xhr) {
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  function onRetweet(tweetId, tweetData) {


    try{       
      var userUUID  = localStorage["uuId"];
      var authToken = localStorage["token"];
      console.log("tweetContent -- " + tweetData);
      var postUrl    = `${API_URL}/users/${userUUID}/tweets/${tweetId}/retweet`;
      // var postUrl    = "http://localhost:8080/api/users/"+userUUID+"/tweets/"+tweetId+"/retweet";
      var tmpDataNew = {"content": tweetData, "status": 'Enabled'};
      console.log("tmpDataNew -- " + JSON.stringify(tmpDataNew));
      var response = null;
      $.ajax({
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tweetData),
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("x-access-token", authToken);
            },
            success : function(result) {
                response = result;                
                if(response.status >= 200 && response.status <= 300){
                    // navigation.push("Home");
                    navigation.push("Home");
                  


                    // setTweetStatusMsg("Your Tweet was sent");
                    // setShowTweetView("false");
                }else{
                    setErrorMsg(response.message);
                }
            },
            error : function(xhr) {
                setErrorMsg(xhr.responseJSON.message);
            }
        });				
      }catch(e){
          alert(e);
      }


  }

  // if (1) {
  //   return (
  //     <View style={styles.container}>
  //       <Button onPress={onToggleSnackBar}>{visible ? "Hide" : "Show"}</Button>
  //       <Snackbar
  //         visible={visible}
  //         onDismiss={onDismissSnackBar}
  //         // action={{
  //         //   label: "Undo",
  //         //   onPress: () => {
  //         //     // Do something
  //         //   },
  //         // }}
  //       >
  //         Hey there! I'm a Snackbar.
  //       </Snackbar>
  //     </View>
  //   );
  // }

  function deleteTweet(tweetId) {
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID +"/tweets/" +tweetId +"/delete";
      var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}/delete`;

      var response = null;
      $.ajax({
        type: "DELETE",
        url: postUrl,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
            //console.log("Tweet Like response -- " + JSON.stringify(response));
            navigation.push("Home");
          }
        },
        error: function (xhr) {
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.subLeftContainer}>
        <img
          src={userImageUrl}
          style={{ width: "43px", height: "43px" }}
          alt={props.tweetPostRow.id}
        />
      </View>

      {
        <View style={styles.subMiddleContainer}>
          <TouchableOpacity
            onPressIn={(e) => {
              e.preventDefault();
              navigateOnTweetDetailsPage(props.tweetPostRow.id);
            }}
          >
            <View style={styles.optionContainer}>
              <Text style={styles.tweetContent}>
                @{tweetUserName} - {Moment(postDate).format("d MMM YYYY")}
                {/* <img
                onClick={(e)=>{e.preventDefault() ;console.log("delete click")}}
                src={optionImage}
                style={{ width: "25px",marginRight:"2px" , height:"15px"}}
                alt="Logo"
              /> */}
              </Text>
            </View>

            <Text style={styles.tweetContent}>
              {props.tweetPostRow.content}
            </Text>
          </TouchableOpacity>

          <View style={styles.replyNLikeContainer}>
            <TouchableOpacity
              onPress={() => showTweetReplyPage(props.tweetPostRow.id)}
              style={styles.linkText}
            >
              <img
                src={replyToImageUrl}
                style={{
                  marginTop: "3px",
                  width: "14px",
                  height: "14px",
                  float: "left",
                }}
                alt={props.tweetPostRow.id}
              />
              <Text lightColor={Colors.light.tint}>
                {props.tweetPostRow.countComment}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => likePost(props.tweetPostRow.id, likeFlag)}
              style={styles.linkText}
            >
              <img
                src={userLikeImageUrl}
                style={{
                  marginTop: "3px",
                  width: "14px",
                  marginRight: "2px",
                  height: "14px",
                  // float: "right",
                }}
                alt={props.tweetPostRow.id}
              />
              <Text lightColor={Colors.light.tint}>
                {" "}
                {props.tweetPostRow.countLike}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => onRetweet(props.tweetPostRow.id, props.tweetPostRow)}
              style={styles.linkText}
            >
              <img
                src={retweetImage}
                style={{
                  marginTop: "3px",
                  width: "18px",
                  marginRight: "2px",
                  height: "20px",
                  // float: "right",
                }}
                alt={props.tweetPostRow.id}
              />
              <Text lightColor={Colors.light.tint}>
                {" "}
                {props.tweetPostRow.countRetweet}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    marginBottom: "25px",
  },
  replyContainer: {
    flexDirection: "row",
    width: "100%",
  },
  replyingTo: {
    width: "90%",
  },
  replyLeftContainer: {
    flexDirection: "column",
    width: "5%",
    marginTop: "10px",
  },
  replyMiddleContainer: {
    flexDirection: "column",
    width: "90%",
    minHeight: "auto",
    marginTop: "10px",
    marginLeft: "20px",
  },
  subLeftContainer: {
    flexDirection: "column",
    marginTop: "20px",
    width: "7%",
  },
  subMiddleContainer: {
    flexDirection: "column",
    marginLeft: "20px",
    marginTop: "20px",
    width: "95%",
    minHeight: "auto",
  },
  optionContainer: {
    display: "flex",
    justifyContent: "space-between",
  },
  replyNLikeContainer: {
    flexDirection: "row",
    marginBottom: "5px",
  },
  tweetReplyContainer: {
    marginTop: "10px",
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#FFF",
  },
  TweetCommentListContainer: {
    flexDirection: "column",
    marginTop: "5px",
    marginBottom: "5px",
    maxHeight: "160px",
  },
  title: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: "10px",
    width: "70%",
  },
  tweetContent: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "90%",
    marginLeft: "2px",
    display: "flex",
    justifyContent: "space-between",
  },
  linkText: {
    flexDirection: "row",
    maxWidth: "70px",
    flex: 1,
    // justifyContent: "space-between",
  },
});
