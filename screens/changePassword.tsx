import * as WebBrowser from "expo-web-browser";
import { useState } from "react";
import { Snackbar } from "react-native-paper";
import {
  TextInput,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import HeaderDataCard from "../screens/HeaderDataCard";

import EditScreenInfo from "../components/EditScreenInfo";
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import * as $ from "jquery";

import logoUrl from "../assets/images/logo.png";
import { API_URL } from "../constants/api";

export default function ChangePassword({ navigation }) {
  const [errorMsg, setErrorMsg] = useState("");
  const [statusMsg, setStatusMsg] = useState("");
  const [isRegistraionSuccess, SetIsRegistraionSuccess] = useState(false);
  const [passwordData, setPasswordData] = useState({
    oldPassword: "",
    password: "",
    confirmPassword: "",
  });

  const [visible, setVisible] = useState(false);
  const onToggleSnackBar = () => setVisible(!visible);
  const onDismissSnackBar = () => setVisible(false);

  const onInputChange = (key, value) => {
    console.log(key, value);
    passwordData[key] = value;
    setPasswordData({ ...passwordData });
    console.log(passwordData);
    if (passwordData.password !== passwordData.confirmPassword) {
      setErrorMsg("Password not matched");
    } else {
      setErrorMsg("");
    }
    // validateField(value, key);
  };

  function resetPassword(password) {
    var emailId = localStorage["userEmailForForgotPass"];
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/password";
      var postUrl = `${API_URL}/users/${userUUID}/password`;

      var tmpData = {
        oldPassword: passwordData.oldPassword,
        password: passwordData.password,
      };
      //console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: postUrl,
        data: JSON.stringify(tmpData),
        dataType: "json",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          console.log("response -- " + JSON.stringify(response));
          if (response.status >= 200 && response.status <= 300) {
            setErrorMsg("");
            setVisible(true);
            // SetIsRegistraionSuccess(true);
            if (!visible) {
              navigation.push("Home");
            }
          } else {
            setErrorMsg(response.message);
          }
        },
        error: function (xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  if (isRegistraionSuccess) {
    return (
      <View style={styles.container}>
        <img
          src={logoUrl}
          style={{ width: 70, margin: "0px auto", float: "left" }}
          alt="Logo"
        />
        <Text style={styles.signUpLinkText}>Change Password Successfully</Text>

        <TouchableOpacity
          onPress={() => navigation.navigate("Home")}
          style={styles.helpLink}
        >
          <Text style={styles.signUpLinkText} lightColor={Colors.light.tint}>
            Back to home!
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  if (visible) {
    return (
      <View style={styles.container}>
        {/* <Button onPress={onToggleSnackBar}>{visible ? "Hide" : "Show"}</Button> */}
        <Snackbar
          visible={visible}
          onDismiss={onDismissSnackBar}
          // action={{
          //   label: "Undo",
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
        >
          Password Changed Successfully
        </Snackbar>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View>
        <HeaderDataCard key="headerTabData" navigation={navigation} />
      </View>

      <Text style={styles.title}>Change your password </Text>
      <View style={styles.errorMsgContainer}></View>

      <View style={styles.statusMsgContainer}>
        <Text style={styles.statusMsgCls}>{statusMsg}</Text>
      </View>
      <View>
        <TextInput
          name="oldPassword"
          secureTextEntry={true}
          style={styles.textInputField}
          placeholder="Current password"
          value={passwordData.oldPassword}
          onChange={(e) => onInputChange("oldPassword", e.target.value)}
        />
        <TextInput
          name="password"
          secureTextEntry={true}
          style={styles.textInputField}
          placeholder="New password"
          value={passwordData.password}
          onChange={(e) => onInputChange("password", e.target.value)}
        />
        <TextInput
          name="confirmPassword"
          secureTextEntry={true}
          style={styles.textInputField}
          placeholder="Confirm new password"
          value={passwordData.confirmPassword}
          onChange={(e) => onInputChange("confirmPassword", e.target.value)}
        />
        <Text style={styles.errorMsgCls}>{errorMsg}</Text>
      </View>

      <View style={styles.helpContainer}>
        <TouchableOpacity
          disabled={errorMsg != ""}
          onPress={() => resetPassword()}
          style={styles.signInActionButton}
        >
          <Text style={{ fontSize: 17, color: "#fff", textAlign: "center" }}>
            Change Password
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: "center",
    paddingLeft: "34px",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: "20px",
    marginLeft: "34px",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    margin: "20px",
    width: "80%",
    textAlign: "center",
  },
  errorMsgContainer: {
    alignItems: "center",
    marginTop: "10px",
  },
  errorMsgCls: {
    fontSize: 15,
    color: "red",
  },
  statusMsgContainer: {
    alignItems: "center",
    marginTop: "10px",
  },
  statusMsgCls: {
    fontSize: 15,
    color: "green",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "150px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "7px",
    marginRight: "20px",
    height: "40px",
    borderRadius: 999999,
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "7px",
    height: "40px",
    display: "flex",
  },
});
