import * as WebBrowser from "expo-web-browser";
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import logoutImageUrl from "../assets/images/logout.jpg";
import userImageUrl from "../assets/images/user.jpg";
import { API_URL } from "../constants/api";

export default function UserDataCard(props) {

const [errorMsg, setErrorMsg] = useState("");
const [sentRequestMsg, setSentRequestMsg] = useState("Send Request");
var userFollowStatus = props.userRow.followType;
var userFollowNewStatus = <TouchableOpacity  onPress={() => sendRequestToUserAction(props.userRow.id)}style={styles.linkText}> <Text  style={{ marginLeft: '10px', marginTop: '10px'}} lightColor={Colors.light.tint}>{sentRequestMsg}</Text> </TouchableOpacity>;
var followerStatusTxt = "";

if(userFollowStatus == "following"){
    userFollowNewStatus = <TouchableOpacity  onPress={() => followingAction(props.userRow.id)}style={styles.linkText}> <Text  style={{ marginLeft: '10px', marginTop: '10px'}} lightColor={Colors.light.tint}>Following</Text> </TouchableOpacity>;
}
if(userFollowStatus == "follower"){
    userFollowNewStatus  = <TouchableOpacity  onPress={() => followBackAction(props.userRow.id)}style={styles.linkText}> <Text  style={{ marginLeft: '10px', marginTop: '10px'}} lightColor={Colors.light.tint}>Follow Back</Text> </TouchableOpacity>;
    followerStatusTxt = "Follows you";
}

function followingAction(userId){
    // alert("followingAction -- " + userId);
}

function followBackAction(userId){
    // alert("followBackAction -- " + userId);
}

function sendRequestToUserAction(userId){
  alert("sendRequestToUserAction -- " + userId);
  try{
        var userUUID  = localStorage["uuId"];
        var authToken = localStorage["token"];       
        
        // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/sentRequest";
        var postUrl   = `${API_URL}/users/${userUUID}/sentRequest`;

        var tmpData   = {"requestSentTo": userId};

        console.log("authToken - " + authToken);        
        console.log("userId - " + userId);

        var response = null;
        $.ajax({
              type: "POST",
              contentType: "application/json",
              url: postUrl,
              data: JSON.stringify(tmpData),
              dataType: "json",
              beforeSend: function (xhr) {
                    xhr.setRequestHeader("x-access-token", authToken);
              },
              success : function(result) {
                  response = result;
                  if(response.status >= 200 && response.status <= 300){
                    setSentRequestMsg(response.message); 
                  }else{
                    setErrorMsg(response.message);
                  }
              },
              error : function(xhr) {
                  setErrorMsg(xhr.responseJSON.message);
              }
          });				
    }catch(e){
        alert(e);
    }
}
  
return (
    <View style={styles.container}>
      <View style={styles.subLeftContainer}>
        <img src={userImageUrl} style={{ width: '43px', height: '43px'}} alt={props.userRow.name} />
      </View>

      <View style={styles.subMiddleContainer}>
        <Text style={styles.title}>{props.userRow.firstName} {props.userRow.lastName} </Text>        
        <Text style={styles.errMsg}>{errorMsg}</Text>

      </View>

      <View style={styles.subRightContainer}>{userFollowNewStatus}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
    flexDirection: "row",
  },
  subLeftContainer: {
    flexDirection: "row",
    marginLeft: '34px',
    marginTop: '20px',
    width: "8%",
    maxHeight: '70px !important',
  },
  subMiddleContainer: { 
    flexDirection: "column",
    marginLeft: '20px',
    marginTop: '20px',
    width: "40%",
    maxHeight: '70px !important',
  },
  subRightContainer: {
    flexDirection: "column",
    marginLeft: '10px',
    marginTop: '20px',
    width: "45%",
    maxHeight: '70px !important',
  },

  title: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
    marginTop: '10px',
  },
  succssMsg: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
    marginTop: '10px',
    color: 'green',
  },
  errMsg: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
    color: 'red',
  },
  linkText: {
    flexDirection: "row"
  },
});
