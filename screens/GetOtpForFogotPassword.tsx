import * as WebBrowser from 'expo-web-browser';
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import Colors from '../constants/Colors';
import { Text, View } from '../components/Themed';
import * as $ from 'jquery';

import logoUrl from "../assets/images/logo.png";
import { API_URL } from '../constants/api';

export default function GetOtpForFogotPassword({navigation}) {

  const [errorMsg, setErrorMsg] = useState("");
  const [email, setEmail] = useState("");

  function getOTPOnEmail(navigation, email) {
    try{
      // var postUrl   = "http://localhost:8080/api/forgotPassword";
      var postUrl   = `${API_URL}/forgotPassword`;

      var tmpData = {"email": email};
      console.log("tmpDataNew -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({							
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpData),
            dataType: "json",
            beforeSend: function (xhr) {
                //Send Authorization token after user logged in
            },
            success : function(result) {
                response = result;                
                if(response.status >= 200 && response.status <= 300){
                  setErrorMsg("");
                  localStorage["userEmailForForgotPass"] = email;
                  navigation.push("VerifyOtpForFogotPasswordTab");
                }else{
                  setErrorMsg(response.message);
                }
        },
        error : function(xhr) {
            console.log(xhr.responseJSON.message);
            setErrorMsg(xhr.responseJSON.message);
        }
        });				
      }catch(e){
          alert(e);
      }
  }


  return (
    <View style={styles.container}>     

       <View style={styles.headerContainer}>            
        <img src={logoUrl} style={{ width: 70, margin: '0px auto', float: 'left'}} alt="Logo"   />
       </View> 
      
      <Text style={styles.title}>To reset password enter your email</Text>

      <Text style={styles.errorMsgCls}>{errorMsg}</Text>

      <TextInput name="email" style={styles.textInputField} placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />

      <View style={styles.helpContainer}>
      
            <TouchableOpacity onPress={() => getOTPOnEmail(navigation, email)} style={styles.actionButton} >
              <Text style={{ fontSize: 17, color: '#fff', textAlign: 'center' }}>Next</Text>
            </TouchableOpacity>

            <TouchableOpacity  onPress={() => navigation.push("LoginTab")}style={styles.helpLink}>
              <Text style={styles.helpLinkText} lightColor={Colors.light.tint}>
                Back to Login?
              </Text>
            </TouchableOpacity>

      </View>

    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: '20px',
    width: '80%'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  errorMsgContainer: {
    alignItems: 'center',
    marginTop: '50px'
  },
  errorMsgCls: {
    fontSize: 15,
    color: 'red',
    textAlign: 'left',
    marginBottom: '10px',
  },
  textInputField: {
    width: '81%',
    padding: '10px',
    textAlign: 'left',
    marginTop: '10px',
    borderStyle: 'solid',
    borderColor: '#ccc', 
    borderWidth: 1
  },
  helpContainer: {    
    alignItems: 'center',
    marginTop: '150px'
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
  actionButtonContainer: {
    flexDirection: 'row'
  },
  actionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '151px', 
    paddingTop: '7px', 
    marginRight: '20px',
    borderRadius: '9999px', 
    height: '40px',
    display: 'flex'
  }

});
