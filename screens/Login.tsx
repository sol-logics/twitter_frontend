import * as WebBrowser from "expo-web-browser";
import { useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from "jquery";

import logoUrl from "../assets/images/logo.png";
import Loader from "./Loader";
import MyComponent from "./SnackComponent";
import showDialog from './Dialog';
import AlertExample from "./alert";
import ToastApp from "./toast";
import { API_URL } from "../constants/api";

/*export default function Login({
  navigation,
}: RootTabScreenProps<"LoginTab">) {*/

export default function Login({ navigation }) {
  localStorage["userEmail"] = ""; //Will be set just after signup user

  const [errorMsg, setErrorMsg] = useState("");
  const [loading, setLoading] = useState(false);

  const [email, setEmail] = useState("shiva1@gmail.com");
  const [password, setPassword] = useState("admin");

  function getTweetPostList(navigation) {
    var userUUID = localStorage["uuId"];
    var authToken = localStorage["token"];

    var postUrl = `${API_URL}/users/${userUUID}/tweets`;

    var tmpData = { countFlag: false, status: "Enabled", tweetTab: "tweets" };
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: postUrl,
      data: tmpData,
      beforeSend: function (xhr) {
        xhr.setRequestHeader("x-access-token", authToken);
      },
      success: function (response) {
        if (response.status >= 200 && response.status <= 300) {
          var tweetsPostList = response.data;
          localStorage["tweetsList"] = JSON.stringify(tweetsPostList);
          localStorage["totalTweets"] = tweetsPostList.length;

          navigation.push("Home");
        }
      },
      error: function (xhr) {
        console.log(xhr.responseJSON.message);
        localStorage["tweetsList"] = "";
        localStorage["totalTweets"] = "0";
        navigation.push("Home");
      },
    });
  }

  function signInUser(navigation, email, password) {
    try {
      var postUrl  = `${API_URL}/signin`
      var tmpData = { email: email, password: password };
      console.log("tmpData Login -- " + JSON.stringify(tmpData));
      var response = null;
      setLoading(true);
      $.ajax({
        type: "POST",
        contentType: "application/json",
        url: postUrl,
        data: JSON.stringify(tmpData),
        dataType: "json",
        beforeSend: function (xhr) {
          //Send Authorization token after user logged in
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
            var loginUserData = response.data;
            localStorage["uuId"] = loginUserData.uuId;
            localStorage["email"] = loginUserData.email;
            localStorage["token"] = loginUserData.token;
            localStorage["password"] = loginUserData.password;

            localStorage["name"] = "";
            if (loginUserData.firstName != null) {
              localStorage["name"] += loginUserData.firstName;
            }
            if (loginUserData.lastName != null) {
              localStorage["name"] += " " + loginUserData.lastName;
            }
            if (
              loginUserData.firstName == null &&
              loginUserData.lastName == null
            ) {
              localStorage["name"] = "User";
            }

            localStorage["profileStatus"] = loginUserData.profileStatus;
            localStorage["active"] = loginUserData.active;
            setErrorMsg("");
            setLoading(false);
            getTweetPostList(navigation);
          } else {
            setErrorMsg(response.message);
            setLoading(false);
          }
        },
        error: function (xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
          setLoading(false);
        },
      });
    } catch (e) {
      alert(e);
      setLoading(false);
    }
    // setLoading(false)
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <img
          src={logoUrl}
          style={{ width: 70, margin: "0px auto", float: "left" }}
          alt="Logo"
        />
      </View>

      <Text style={styles.title}>
        See what's <br />
        happening in the <br />
        world right now
      </Text>

      <Text style={styles.errorMsgCls}>{errorMsg}</Text>

      <TextInput
        name="email"
        style={styles.textInputField}
        placeholder="Email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextInput
        name="password"
        secureTextEntry={true}
        style={styles.textInputField}
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />


      <View style={styles.helpContainer}>
        <TouchableOpacity
          onPress={() => signInUser(navigation, email, password)}
          style={styles.signInActionButton}
        >
          <Text style={{ fontSize: 17, color: "#fff", textAlign: "center" }}>
            Sign In
          </Text>
        </TouchableOpacity>
        {/* <ToastApp /> */}
        <View style={styles.signUpContainer}>
          <Text
            style={{
              fontSize: 15,
              color: "#000",
              textAlign: "center",
              marginTop: "15px",
            }}
          >
            If you haven't account,{" "}
          </Text>
         {/* <AlertExample  title={"Delete Confirm!"} desc={"delete?"}/> */}
          <TouchableOpacity
            onPress={() => navigation.push("SignupTab")}
            style={styles.helpLink}
          >
            <Text style={styles.signUpLinkText} lightColor={Colors.light.tint}>
              {" "}
              Sign up now
            </Text>
          </TouchableOpacity>
        
        </View>
        {/* <MyComponent /> */}
        <TouchableOpacity
          onPress={() => navigation.push("GetOtpForFogotPasswordTab")}
          style={styles.helpLink}
        >
          <Text style={styles.signUpLinkText} lightColor={Colors.light.tint}>
            Forgot password?
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: "20px",
    marginLeft: "34px",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    margin: "20px",
    textAlign: "left",
    marginLeft: "0px",
    //borderColor: 'red',
    //borderWidth: 1,
    width: "75%",
  },
  errorMsgCls: {
    fontSize: 15,
    color: "red",
    textAlign: "left",
    marginBottom: "10px",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 9999,
    height: "34px",
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "5px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
});
