import * as WebBrowser from 'expo-web-browser';
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import Colors from '../constants/Colors';
import { Text, View } from '../components/Themed';
import * as $ from 'jquery';

import logoUrl from "../assets/images/logo.png";
import { API_URL } from '../constants/api';

export default function Signup({navigation}) {

  const [errorMsg, setErrorMsg] = useState("");
  const [statusMsg, setStatusMsg] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function resetPassword(password) {
    var  emailId = localStorage["userEmailForForgotPass"];
    try{
      // var postUrl   = "http://localhost:8080/api/resetPassword";
      var postUrl   = `${API_URL}/resetPassword`;
      var tmpData = {"email": emailId, "password": password};
      //console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({							
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpData),
            dataType: "json",
            beforeSend: function (xhr) {
                //Send Authorization token after user logged in
            },
            success : function(result) {
                response = result;
                console.log("response -- " + JSON.stringify(response));
                if(response.status >= 200 && response.status <= 300){             
                  setErrorMsg("");
                  navigation.push("SetNewPasswordSuccessTab");
                }else{                  
                  setErrorMsg(response.message);
                }
        },
        error : function(xhr) {
            console.log(xhr.responseJSON.message);
            setErrorMsg(xhr.responseJSON.message);
        }
        });				
      }catch(e){
          alert(e);
      }
  }


  return (
    <View style={styles.container}>

       <View style={styles.headerContainer}>            
        <img src={logoUrl} style={{ width: 70, margin: '0px auto', float: 'left'}} alt="Logo"   />
       </View>
      
      <Text style={styles.title}>Reset your password </Text>

      <View style={styles.errorMsgContainer}>
      <Text style={styles.errorMsgCls}>{errorMsg}</Text>
      </View>

      <View style={styles.statusMsgContainer}>
      <Text style={styles.statusMsgCls}>{statusMsg}</Text>
      </View>


      <TextInput name="password" secureTextEntry={true} style={styles.textInputField} placeholder="New password" value={password} onChange={(e) => setPassword(e.target.value)} />
      <TextInput name="confirmPassword" secureTextEntry={true} style={styles.textInputField} placeholder="Confirm new password" />

      <View style={styles.helpContainer}>  

            <TouchableOpacity onPress={() => resetPassword(password)} style={styles.signInActionButton}>
              <Text style={{ fontSize: 17, color: '#fff', textAlign: 'center' }}>Update Password</Text>
            </TouchableOpacity>

            <TouchableOpacity  onPress={() => navigation.push("LoginTab")}style={styles.helpLink}>
              <Text style={styles.helpLinkText} lightColor={Colors.light.tint}>
                Back to Login?
              </Text>
            </TouchableOpacity>

      </View>

    </View>
  );
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/get-started/create-a-new-app/#opening-the-app-on-your-phonetablet'
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: '20px',
    width: '80%'
  },
  errorMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  errorMsgCls: {
    fontSize: 15,
    color: 'red',
  },
  statusMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  statusMsgCls: {
    fontSize: 15,
    color: 'green',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  textInputField: {
    width: '81%',
    padding: '10px',
    textAlign: 'left',
    marginTop: '10px',
    borderStyle: 'solid',
    borderColor: '#ccc', 
    borderWidth: 1
  },
  helpContainer: {    
    alignItems: 'center',
    marginTop: '150px'
  },
  actionButtonContainer: {
    flexDirection: 'row'
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
  signUpContainer: {
    flexDirection: 'row'
  },
  signUpLinkText: {
    textAlign: 'center',
    fontSize: 15
  },
  signInActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '200px', 
    paddingTop: '7px', 
    marginRight: '20px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  },
  signUpActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '200px',
    paddingTop: '7px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  }


});
