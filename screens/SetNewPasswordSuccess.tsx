import * as WebBrowser from 'expo-web-browser';
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import Colors from '../constants/Colors';
import { Text, View } from '../components/Themed';
import * as $ from 'jquery';

import logoUrl from "../assets/images/logo.png";

export default function Signup({navigation}) {
    return (
      <View style={styles.container}>  

       <View style={styles.headerContainer}>            
        <img src={logoUrl} style={{ width: 70, margin: '0px auto', float: 'left'}} alt="Logo"   />
       </View>

        <Text style={styles.title}></Text>
        <Text style={styles.statusMsgCls}>Password has been reset successfully</Text>      
          <TouchableOpacity  onPress={() => navigation.push("LoginTab")}style={styles.helpLink}>
            <Text style={styles.signinLinkText} lightColor={Colors.light.tint}>click here to Login</Text>
          </TouchableOpacity>      
      </View>
    );  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: '20px',
    width: '80%'
  },
  errorMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  errorMsgCls: {
    fontSize: 15,
    color: 'red',
  },
  statusMsgCls: {
    fontSize: 15,
    color: 'green',
    marginTop: '15px'
  },
  signinLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  statusMsgContainer: {
    alignItems: 'center',
    marginTop: '10px'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  textInputField: {
    width: '81%',
    padding: '10px',
    textAlign: 'left',
    marginTop: '10px',
    borderStyle: 'solid',
    borderColor: '#ccc', 
    borderWidth: 1
  },
  helpContainer: {    
    alignItems: 'center',
    marginTop: '150px'
  },
  actionButtonContainer: {
    flexDirection: 'row'
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
  signUpContainer: {
    flexDirection: 'row'
  },
  signUpLinkText: {
    textAlign: 'center',
    fontSize: 15
  },
  signInActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '200px', 
    paddingTop: '5px', 
    marginRight: '20px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  },
  signUpActionButton: {
    backgroundColor: 'rgb(29, 155, 240)', 
    width: '200px', 
    paddingTop: '5px',
    borderRadius: 9999, 
    height: '40px',
    display: 'flex'
  }


});
