import * as WebBrowser from "expo-web-browser";
import { useEffect, useState } from "react";
import useDeepCompareEffect from "use-deep-compare-effect";
import {
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Moment from "moment";
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import optionImage from "../assets/images/options.png";
import { RootTabScreenProps } from "../types";
import * as $ from "jquery";
import TweetCommentDataCard from "../screens/TweetCommentDataCard";
import HeaderDataCard from "../screens/HeaderDataCard";
import userImageUrl from "../assets/images/user.jpg";
import likeImageUrl from "../assets/images/likes.png";
import replyToImageUrl from "../assets/images/twitter_reply.png";
import { Button, Paragraph, Dialog, Portal } from "react-native-paper";
import { API_URL } from "../constants/api";

export default function TweetPostDetailsDataCard(props) {
  Moment.locale("en");

  var navigation = props.navigation;
  const { schoolId, classId } = props;

  //var tweetId = props.route.params.tweetId;

  const [tweetId, setTweetId] = useState(props.route.params.tweetId);

  const [tweetUserName, setTweetUserName] = useState(localStorage["name"]);
  const [tweetCommentList, setTweetCommentList] = useState([]);
  const [countTweetLikes, setCountTweetLikes] = useState(0);
  const [countTweetComments, setCountTweetComments] = useState(0);

  const [tweetcontents, setTweetcontents] = useState("");
  const [tweetDate, setTweetDate] = useState("");

  const [errorMsg, setErrorMsg] = useState("");
  const [tweetReply, setTweetReply] = useState("");

  useEffect(() => {
    console.log(props);
    // if (localStorage["uuId"] != "" && localStorage["token"] != "") {
    getTweetDetailsBytweetId(props.route.params.tweetId);
    getTweetCommentList(props.route.params.tweetId);
    // }
  }, [props.route.params.tweetId]);

  function showTweetReplyPage(navigation, tweetId) {
    navigation.push("ReplyTweetTab", { tweetId });
  }

  function OnDeleteTweeet(tweetId) {
    console.log("delete call F", tweetId);
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID +"/tweets/" + tweetId + "/delete";
      var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}/delete`;
      var response = null;
      $.ajax({
        type: "DELETE",
        url: postUrl,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
            //console.log("Tweet Like response -- " + JSON.stringify(response));
            navigation.push("Home");
          }
        },
        error: function (xhr) {
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  function getTweetCommentList(tweetId) {
    var userUUID = localStorage["uuId"];
    var authToken = localStorage["token"];

    // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/tweets/" + tweetId +"/comments";
    var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}/comments`;

    var tmpData = {
      countFlag: false,
      commentStatus: "Enabled",
      commentParentId: 0,
    };

    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: postUrl,
      data: tmpData,
      beforeSend: function (xhr) {
        xhr.setRequestHeader("x-access-token", authToken);
      },
      success: function (response) {
        if (response.status >= 200 && response.status <= 300) {
          var tweetCommentListData = response.data;
          setTweetCommentList(tweetCommentListData);
        }
      },
      error: function (xhr) {
        setErrorMsg(xhr.responseJSON.message);
      },
    });
  }

  function show() {
    // if (tweetId === 12, e) {
    //   alert(true);
    //   e.preventDefault();
    // } else {
    //   alert(false);
    // }
  }

  //   window.onload = ()  =>{
  //     getTweetDetailsBytweetId(tweetId);
  //     getTweetCommentList(tweetId);
  // }

  // getTweetDetailsBytweetId(tweetId);
  // getTweetCommentList(tweetId);

  function likePost(tweetId) {
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" +userUUID +"/tweets/" +tweetId +"/like";
      var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}/like`;

      var response = null;
      $.ajax({
        type: "POST",
        url: postUrl,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          if (response.status >= 200 && response.status <= 300) {
          }
        },
        error: function (xhr) {
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  function getTweetDetailsBytweetId(tweetId) {
    var userUUID = localStorage["uuId"];
    var authToken = localStorage["token"];
    // var postUrl ="http://localhost:8080/api/users/" + userUUID + "/tweets/" + tweetId;
    var postUrl = `${API_URL}/users/${userUUID}/tweets/${tweetId}`;
    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: postUrl,
      beforeSend: function (xhr) {
        xhr.setRequestHeader("x-access-token", authToken);
      },
      success: function (response) {
        if (response.status >= 200 && response.status <= 300) {
          setTweetcontents(response.data[0].content);
          setCountTweetComments(response.data[0].countComment);
          setCountTweetLikes(response.data[0].countLike);
          setTweetDate(response.data[0].createdDate);
        }
      },
      error: function (xhr) {
        setErrorMsg(xhr.responseJSON.message);
      },
    });
  }

  // getTweetDetailsBytweetId(tweetId);
  // getTweetCommentList(tweetId);

  // useDeepCompareEffect(() => {
  //   if (localStorage["uuId"] != "" && localStorage["token"] != "") {
  //     getTweetDetailsBytweetId(tweetId);
  //     getTweetCommentList(tweetId);
  //   }
  // }, []);

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <View style={styles.headerContainer}>
          <HeaderDataCard key="headerTabData" navigation={navigation} />
        </View>

        <View style={styles.subBodyContainer}>
          <View style={styles.subLeftContainer}>
            <img
              src={userImageUrl}
              style={{ width: "43px", height: "43px" }}
              alt={tweetId}
            />
          </View>

          <View style={styles.subMiddleContainer}>
            <Text style={styles.tweetContentCss}>
              @{tweetUserName} - {Moment(tweetDate).format("d MMM YYYY")}
              <img
                onClick={(e) => {
                  e.preventDefault();
                  // console.log("delete click");
                  OnDeleteTweeet(tweetId);
                }}
                src={optionImage}
                style={{
                  width: "25px",
                  marginRight: "2px",
                  height: "15px",
                  float: "right",
                }}
                alt="Logo"
              />
            </Text>
            <Text style={styles.tweetContentCss}>{tweetcontents}</Text>

            <View style={styles.replyNLikeContainer}>
              <TouchableOpacity
                onPress={() => showTweetReplyPage(navigation, tweetId)}
                style={styles.linkText}
              >
                <img
                  src={replyToImageUrl}
                  style={{
                    marginTop: "3px",
                    width: "14px",
                    height: "14px",
                    float: "left",
                  }}
                  alt={tweetId}
                />
                <Text lightColor={Colors.light.tint}>
                  {" "}
                  {countTweetComments}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => likePost(tweetId)}
                style={styles.linkText}
              >
                <img
                  src={likeImageUrl}
                  style={{
                    marginTop: "3px",
                    width: "14px",
                    height: "14px",
                    float: "left",
                  }}
                  alt={tweetId}
                />
                <Text lightColor={Colors.light.tint}> {countTweetLikes}</Text>
              </TouchableOpacity>
            </View>

            <ScrollView style={styles.TweetCommentListContainer}>
              {tweetCommentList.length >= 1 ? (
                tweetCommentList.map((tweetCommentRow, index) => (
                  <TweetCommentDataCard
                    key={index}
                    navigationData={navigation}
                    tweetCommentRow={tweetCommentRow}
                  />
                ))
              ) : (
                <Text style={styles.title}>No comment yet</Text>
              )}
            </ScrollView>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: "15px",
  },
  tweetContainer: {
    flexDirection: "row",
    marginTop: "15px",
  },
  subContainer: {
    marginLeft: "34px",
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: "15px",
  },
  TweetPostListContainer: {
    flexDirection: "column",
    marginTop: "20px",
    maxHeight: "430px",
  },
  replyContainer: {
    flexDirection: "row",
    width: "100%",
  },
  replyingTo: {
    width: "94%",
  },
  replyLeftContainer: {
    flexDirection: "column",
    width: "5%",
    marginTop: "10px",
  },
  replyMiddleContainer: {
    flexDirection: "column",
    width: "94%",
    minHeight: "auto",
    marginTop: "10px",
    marginLeft: "20px",
  },
  subBodyContainer: {
    flexDirection: "row",
  },
  subLeftContainer: {
    flexDirection: "column",
    marginTop: "20px",
    width: "5%",
  },
  subMiddleContainer: {
    flexDirection: "column",
    marginLeft: "20px",
    marginTop: "20px",
    width: "94%",
    minHeight: "auto",
  },
  replyNLikeContainer: {
    flexDirection: "row",
  },
  dialogContainer: {
    borderWidth: 1,
    borderColor: "#ccc",
    backgroundColor: "#FFF",
  },
  TweetCommentListContainer: {
    flexDirection: "column",
    marginTop: "5px",
    marginBottom: "5px",
    maxHeight: "367px",
  },
  title: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: "10px",
    width: "70%",
  },
  tweetContentCss: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "94%",
  },
  linkText: {
    flexDirection: "row",
    maxWidth: "70px",
    flex: 1,
  },
});
