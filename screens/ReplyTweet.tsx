import * as WebBrowser from "expo-web-browser";
import {useState, useEffect } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";
import Moment from 'moment';
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';
import userImageUrl from "../assets/images/user.jpg";
import { API_URL } from "../constants/api";


export default function TweetReplyPage(props) {

  const [tweetReply, setTweetReply] = useState("");
  const [tweetStatusMsg, setTweetStatusMsg] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  var navigation = props.navigation;
  var tweetId = props.route.params.tweetId;  
  const [tweetUserName, setTweetUserName] = useState("");
  const [tweetcontents, setTweetcontents] = useState("");
  const [tweetDate, setTweetDate] = useState("");  

  function replyTweetPost(tweetId, tweetReplyContent) {
    
    var userUUID = localStorage["uuId"];
    var authToken = localStorage["token"];    
    // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/tweets/" + tweetId + "/comments";
    var postUrl   = `${API_URL}/users/${userUUID}/tweets/${tweetId}/comments`;

    var tmpData = { comment: tweetReplyContent, commentParentId: 0, commentStatus: "Enabled" };

    $.ajax({
                  type: "POST",
                  contentType: "application/json",
                  url: postUrl,
                  data: JSON.stringify(tmpData),
                  dataType: "json",
                  beforeSend: function (xhr) {
                    xhr.setRequestHeader("x-access-token", authToken);
                  },
                  success: function (response) {
                    if (response.status >= 200 && response.status <= 300) {     
                        navigation.push("Home");
                    }
                  },
                  error: function (xhr) {
                    setErrorMsg(xhr.responseJSON.message);
                  },
            });

  }

  function getTweetDetailsBytweetId(tweetId){

        var userUUID  = localStorage["uuId"];
        var authToken = localStorage["token"];
        // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/tweets/"+tweetId;
        var postUrl   = `${API_URL}/users/${userUUID}/tweets/${tweetId}`;
        
        $.ajax({
              type: "GET",
              contentType: "application/json",
              url: postUrl,
              beforeSend: function (xhr) {
                xhr.setRequestHeader("x-access-token", authToken);
              },
              success : function(response) {

                  if(response.status >= 200 && response.status <= 300){
                    setTweetcontents(response.data[0].content);
                    setTweetDate(response.data[0].createdDate);     
                    setTweetUserName(response.data[0].userName)               
                  }
              },
              error : function(xhr) {
                  setErrorMsg(xhr.responseJSON.message);                  
              }
          });
  }

  //  getTweetDetailsBytweetId(tweetId);  
  
   useEffect(() => {
    getTweetDetailsBytweetId(tweetId);
  }, [tweetId]);

 return (
    <View style={styles.container}>      
                <View style={styles.replyContainer}>
                    <View style={styles.replyLeftContainer}>
                      <img src={userImageUrl} style={{ width: "43px", height: "43px" }} alt={tweetId} />
                    </View>
                    <View style={styles.replyMiddleContainer}>
                        <Text style={styles.tweetContent}>@{tweetUserName} - {Moment(tweetDate).format("d MMM YYYY")}</Text>
                        <Text style={styles.tweetContent}>{tweetcontents}</Text>
                    </View>
                </View>

                <View style={styles.replyContainer}>
                    <View style={styles.replyLeftContainer}></View>
                    <View style={styles.replyMiddleContainer}>
                        <Text style={styles.replyingTo} lightColor={Colors.light.tint}>Replying to @{tweetUserName}</Text>
                    </View>                            
                </View>

                <View style={styles.replyContainer}>
                    <View style={styles.replyLeftContainer}>
                      <img src={userImageUrl} style={{ width: "43px", height: "43px" }} alt={tweetId} />
                    </View>
                    <View style={styles.replyMiddleContainer}>
                          <textarea name="tweetReplys" style={{marginLeft: '0px', borderWidth: '0', padding: '10px', width: '88%', height: '70px', fontSize: 15, color: "#000", textAlign: "left" }} placeholder="Tweet your reply" value={tweetReply} onChange={(e) => setTweetReply(e.target.value)}></textarea>
                    </View>
                </View>

                <View style={styles.actionButtonContainer}>
                    <TouchableOpacity onPress={() => navigation.push("Home")} style={styles.actionButtonClose}>
                              <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Close</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => replyTweetPost(tweetId, tweetReply)} style={styles.actionButtonReply}>
                        <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Reply</Text>
                    </TouchableOpacity>
                </View>                
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '4%',
  },
  replyContainer: {
    flexDirection: "row",    
  },
  replyingTo: {
    width: "90%",
  },
  replyLeftContainer: {
    flexDirection: "column",
    width: "5%",
    marginTop: "10px",    
  },
  replyMiddleContainer: {
    flexDirection: "column",
    width: "90%",
    minHeight: "auto",
    marginTop: "10px",
    marginLeft: "20px",
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },  
  showPostTweetBtnContainer: {
    width: '97%',
  },
  addPostTweetBtnContainer: {
    width: '97%',
  },
  createPostTweetBtnContainer: {
        width: '40%',
        flexDirection: "row",
        marginLeft: '54%',
  },
  subMiddleContainer: { 
    flexDirection: "row",
    marginLeft: '20px',
    marginTop: '20px',
    width: "98%",
  },  
  textInputField: {
    width: "96%",
    height: "100px",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  actionButtonContainer: {
    flexDirection: 'row',
  },
  actionButtonClose: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "100px",
    paddingTop: "5px",
    paddingBottom: "10px",
    borderRadius: 9999,
    marginTop: '20px',
    marginLeft: '25%',
  },
  actionButtonReply: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "100px",
    paddingTop: "5px",
    paddingBottom: "10px",
    borderRadius: 9999,
    marginTop: '20px',
    marginLeft: '5%',
  },
  title: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: '3px',
    width: "70%",
  },
  tweetContent: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",    
  },
  linkText: {
    flexDirection: "row"
  },
});
