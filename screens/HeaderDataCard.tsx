import * as WebBrowser from "expo-web-browser";
import { useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { Alert, Modal, Pressable } from "react-native";
import Moment from "moment";
import Colors from "../constants/Colors";
import { Text } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from "jquery";
import { View } from "react-native";
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';


import {
  Button,
  Paragraph,
  Dialog,
  Portal,
  Provider,
} from "react-native-paper";
import logoUrl from "../assets/images/logo.png";
import userMenuImageUrl from "../assets/images/user_menu.png";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
export default function TweetCommentDataCard(props) {
  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);
  const [modalVisible, setModalVisible] = useState(false);


  function logoutUser(navigation) {
    // setVisible(true)
    localStorage.clear();
    navigation.navigate ("LoginTab");
    // ShowDialog()/
    // setVisible(true);
    // setModalVisible(true);
  }

  if (visible) {
    return (
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Actions>
            <Button onPress={() => console.log("Cancel")}>Cancel</Button>
            <Button onPress={() => console.log("Ok")}>Ok</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      // <View style={styles.centeredView}>
      //   <Modal
      //     animationType="slide"
      //     transparent={true}
      //     visible={modalVisible}
      //     onRequestClose={() => {
      //       Alert.alert("Modal has been closed.");
      //       setModalVisible(!modalVisible);
      //     }}
      //   >
      //     <View style={styles.centeredView}>
      //       <View style={styles.modalView}>
      //         <Text style={styles.titleText}>Delete</Text>
      //         <Text style={styles.modalText}>Are you sure delete ?</Text>
      //         <View style={styles.actionView}>
      //           <Pressable
      //             style={[styles.button, styles.buttonClose]}
      //             onPress={() => setModalVisible(!modalVisible)}
      //           >
      //             <Text style={styles.textStyle}>Yes </Text>
      //           </Pressable>
      //           <Pressable
      //             style={[styles.button, styles.buttonClose]}
      //             onPress={() => setModalVisible(!modalVisible)}
      //           >
      //             <Text style={styles.textStyle}>No </Text>
      //           </Pressable>
      //         </View>
      //       </View>
      //     </View>
      //   </Modal>
      //   {/* <Pressable
      //     style={[styles.button, styles.buttonOpen]}
      //     onPress={() => setModalVisible(true)}
      //   >
      //     <Text style={styles.textStyle}>Show Modal</Text>
      //   </Pressable> */}
      // </View>
    );
  }
  // function ShowDialog() {
  //   return (
  //     <Provider>
  //       <View>
  //         <Button onPress={showDialog}>Show Dialog</Button>
  //         <Portal>
  //           <Dialog visible={visible} onDismiss={hideDialog}>
  //             <Dialog.Title>Alert</Dialog.Title>
  //             <Dialog.Content>
  //               <Paragraph>This is simple dialog</Paragraph>
  //             </Dialog.Content>
  //             <Dialog.Actions>
  //               <Button onPress={hideDialog}>Done</Button>
  //             </Dialog.Actions>
  //           </Dialog>
  //         </Portal>
  //       </View>
  //     </Provider>
  //   );
  // }

  return (
    <View style={styles.container}>
      <View style={styles.menuContainer}>
        <Menu>
          <MenuTrigger>
            <img
              src={userMenuImageUrl}
              style={{ width: 34, margin: "0px auto" }}
              alt="Logo"
            />
          </MenuTrigger>
          <MenuOptions>
            <MenuOption
              onSelect={() => props.navigation.navigate("Profile")}
              text="Profile"
            />
            <MenuOption
              onSelect={() => props.navigation.navigate("Notifications")}
              text="Notifications"
            />
            <MenuOption
              onSelect={() => props.navigation.navigate("FollowerRequests")}
              text="Follower Requests"
            />
            <MenuOption
              onSelect={() => props.navigation.navigate("ChangePassword")}
              text="Change Password"
            />
            <MenuOption
              onSelect={() => logoutUser(props.navigation)}
              text="Logout"
            />
          </MenuOptions>
        </Menu>
      </View>
      <img src={logoUrl} style={{ width: 70, margin: "0px auto" }} alt="Logo" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
    flexDirection: "row",
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: "20px",
  },
  subLeftContainer: {
    flexDirection: "row",
    marginTop: "20px",
    width: "8%",
    maxHeight: "50px !important",
  },
  subMiddleContainer: {
    flexDirection: "column",
    marginLeft: "20px",
    marginTop: "20px",
    width: "80%",
    maxHeight: "50px !important",
  },
  subRightContainer: {
    flexDirection: "column",
    marginLeft: "10px",
    marginTop: "20px",
    width: "40%",
    maxHeight: "50px !important",
  },

  title: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: "3px",
    width: "70%",
  },
  tweetContent: {
    fontSize: 12,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
  },
  linkText: {
    flexDirection: "row",
  },
  centeredView: {
    flex: 1,

    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    width: "90%",
    alignItems: "center",
    shadowColor: "#000",

    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  actionView: {
    // width:"100px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
    width: "150px",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
    width: "150px",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  titleText: {
    marginBottom: 15,
    fontSize: "18px",
    textStyle: "bold",
    textAlign: "center",
  },
});
