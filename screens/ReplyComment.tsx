import * as WebBrowser from "expo-web-browser";
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";
import Moment from 'moment';
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import addTweetImageUrl from "../assets/images/add_tweet_btn.png";
import { API_URL } from "../constants/api";

export default function TweetCommentDataCard(props) {

  var navigation = props.navigation;
  var tweetId    = props.route.params.tweetId;
  var commentId  = props.route.params.commentId;

  const [commentContent, setCommentContent] = useState("");
  const [commentStatusMsg, setCommentStatusMsg] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  const [showTweetView, setShowTweetView] = useState("false");
  console.log("showTweetView -- " + showTweetView);

  function postTweetCommentReply(navigation, tweetId, commentId, commentContent){
    try{       
      var userUUID  = localStorage["uuId"];
      var authToken = localStorage["token"];
      console.log("commentContent -- " + commentContent);      
      // var postUrl    = "http://localhost:8080/api/users/"+userUUID+"/tweets/"+tweetId+"/comments";
      var postUrl    =`${API_URL}/users/${userUUID}/tweets/${tweetId}/comments`;

      var tmpDataNew = {"comment": commentContent, "commentStatus": 'Enabled', "commentParentId": commentId};
      console.log("tmpDataNew -- " + JSON.stringify(tmpDataNew));
      var response = null;
      $.ajax({
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(tmpDataNew),
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("x-access-token", authToken);
            },
            success : function(result) {
                response = result;                
                if(response.status >= 200 && response.status <= 300){                    
                    setCommentStatusMsg("Your Comment was sent");
                    navigation.push("TweetPostDetailsDataCardTab",{tweetId});
                }else{
                    setErrorMsg(response.message);
                }
            },
            error : function(xhr) {
                setErrorMsg(xhr.responseJSON.message);
            }
        });				
      }catch(e){
          alert(e);
      }
  }
  
 return (
    <View style={styles.container}>
            <View style={styles.addPostTweetBtnContainer}>
                <textarea name="commentContent" style={{padding: '10px', width: '95%', height: '100px', fontSize: 15, color: "#000", textAlign: "left" }} placeholder="What’s happening" value={commentContent} onChange={(e) => setCommentContent(e.target.value)}></textarea>
                
                <View style={styles.createPostTweetBtnContainer}>
                      <TouchableOpacity onPress={() => navigation.push("Home")} style={styles.actionButton}>
                          <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Close</Text>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => postTweetCommentReply(navigation, tweetId, commentId, commentContent)} style={styles.actionButton}>
                          <Text style={{fontSize: 17, color: "#fff", textAlign: "center" }}>Tweet</Text>
                      </TouchableOpacity>
                </View>            
            </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '4%',
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },  
  showPostTweetBtnContainer: {
    width: '97%',
  },
  addPostTweetBtnContainer: {
    width: '97%',
  },
  createPostTweetBtnContainer: {
        width: '40%',
        flexDirection: "row",
        marginLeft: '30%',
  },
  subMiddleContainer: { 
    flexDirection: "row",
    marginLeft: '20px',
    marginTop: '20px',
    width: "98%",
  },  
  textInputField: {
    width: "96%",
    height: "100px",
    padding: "8px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  actionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "100px",
    paddingTop: "5px",
    paddingBottom: "10px",
    borderRadius: 9999,
    marginTop: '20px',
    marginRight: '20px', 
  },
  title: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: '3px',
    width: "70%",
  },
  tweetContent: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",    
  },
  linkText: {
    flexDirection: "row"
  },
});
