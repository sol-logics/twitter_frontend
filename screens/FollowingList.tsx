import * as WebBrowser from "expo-web-browser";
import {useEffect, useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import FollowingDataCard from '../screens/FollowingDataCard';
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import HeaderDataCard from '../screens/HeaderDataCard';
import { API_URL } from "../constants/api";

export default function FollowingList({navigation}) {

  const [errorMsg, setErrorMsg] = useState("");
  const [followingList, setFollowingList] = useState([]);
 
  var uuId = localStorage["uuId"];
  var name = localStorage["name"];
  var authToken = localStorage["token"];
  var email = localStorage["email"];
  var password = localStorage["password"];  
  var profileStatus = localStorage["profileStatus"];
  var active = localStorage["active"];

  function isUserLoggedIn(navigation){
      if(localStorage["email"] == ""){
          navigation.push("LoginTab");
      }
  }
  isUserLoggedIn(navigation);

  function getFollowingList(){

    var userUUID  = localStorage["uuId"];
    var authToken = localStorage["token"];
    // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/following";
    var postUrl   = `${API_URL}/users/${userUUID}/following`;

    var tmpData = {"countFlag": false, "requestStatus": "Confirmed"};

    $.ajax({
          type: "GET",
          contentType: "application/json",
          url: postUrl,
          data: tmpData,
          beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", authToken);
          },
          success : function(response) {
              if(response.status >= 200 && response.status <= 300){
                setFollowingList(response.data);
              }
          },
          error : function(xhr) {            
            console.log(xhr.responseJSON.message);
            setErrorMsg(xhr.responseJSON.message);
        }
      });
  }
  


  useEffect(() => {
    console.log("this is following list")
    getFollowingList();
  }, []);
  

  return (
    <View style={styles.mainContainer}>

          <View style={styles.headerContainer}>
              <HeaderDataCard key="headerTabData"  navigation={navigation} />
          </View>

          <View style={styles.followingListContainer}>
                { followingList.length >= 1 ?  followingList.map((fwList, index) => <FollowingDataCard key={index}  fwList={fwList} />) : <Text style={styles.title}>You aren't following any user yet</Text> }
          </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },
  followingListContainer: {
    flexDirection: "column",
  },
  menuLink: {
    flexDirection: "row",
  },
  PageTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: "34px",
    textAlign: "left",
    width: "75%",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "left",
    marginLeft: "34px",
    marginTop: '3px',
    width: "70%",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  logoutLinkText: {
    textAlign: "left",
    marginTop: '7px',
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
});
