import * as WebBrowser from "expo-web-browser";
import { useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from "jquery";

import logoutImageUrl from "../assets/images/logout.jpg";
import userImageUrl from "../assets/images/user.jpg";
import { API_URL } from "../constants/api";

export default function FollowerRequestCardList(props) {
  const [errorMsg, setErrorMsg] = useState("");
  const [statusMsg, setStatusMsg] = useState("");
  const [isRegistraionSuccess, SetIsRegistraionSuccess] = useState(false);
  console.log(props);

  function onActionRequest(status, userId) {
    console.log(status, userId);
    RequestAction(status, userId);
  }

  function RequestAction(status, userId) {
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/requestAction";
      var postUrl = `${API_URL}/users/${userUUID}/requestAction`;
      var tmpData = {
        memberId: userId,
        requestStatus: status,
      };
      //console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: postUrl,
        data: JSON.stringify(tmpData),
        dataType: "json",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          console.log("response -- " + JSON.stringify(response));
          if (response.status >= 200 && response.status <= 300) {
            setErrorMsg("");
            SetIsRegistraionSuccess(true);
            props.navigation.push("FollowerRequests");
            console.log(props);
          } else {
            setErrorMsg(response.message);
          }
        },
        error: function (xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  return (
    <View>
      <View style={styles.container}>
        <View style={styles.subLeftContainer}>
          <img
            src={userImageUrl}
            style={{ width: "43px", height: "43px" }}
            alt={props.fwList.name}
          />
        </View>

        <View style={styles.subMiddleContainer}>
          <Text style={styles.title}>
            {props.fwList.firstName} {props.fwList.lastName}{" "}
          </Text>
          <Text style={styles.title}>{props.fwList.email}</Text>
        </View>
      </View>
      <View style={styles.actionContainer}>
        {/* <Text  style={{ marginLeft: '10px'}} lightColor={Colors.light.tint}>Confirm</Text>
       <Text  style={{ marginLeft: '10px'}} lightColor={Colors.light.tint}>Reject</Text> */}

        <TouchableOpacity
          onPress={() => onActionRequest("Declined", props.fwList.id)}
          style={styles.DeclineButton}
        >
          <Text style={{ fontSize: 12, color: "#fff", textAlign: "center" }}>
            Decline
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onActionRequest("Confirmed", props.fwList.id)}
          style={styles.AcceptButton}
        >
          <Text style={{ fontSize: 12, color: "#fff", textAlign: "center" }}>
            Accept
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
    flexDirection: "row",
  },

  AcceptButton: {
    backgroundColor: "#30b359",
    width: "80px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 9999,
    height: "28px",
    display: "flex",
  },
  DeclineButton: {
    backgroundColor: "#676e69",
    width: "80px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 9999,
    height: "28px",
    display: "flex",
  },
  subLeftContainer: {
    flexDirection: "row",
    marginLeft: "24px",
    marginTop: "20px",
    width: "8%",
    maxHeight: "50px !important",
  },
  subMiddleContainer: {
    flexDirection: "column",
    marginLeft: "20px",
    marginTop: "20px",
    width: "50%",
    maxHeight: "50px !important",
  },
  subRightContainer: {
    flexDirection: "column",
    marginLeft: "10px",
    marginTop: "20px",
    width: "40%",
    maxHeight: "50px !important",
  },
  actionContainer: {
    // marginLeft: "10px",
    // marginTop: "20px",
    // width: "",
    // display: flex;
    // flex-direction: row;
    display: "flex",
    flexDirection: "row-reverse",
    marginLeft: "34px",
    marginTop: "4px",
  },
  title: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
  },
  linkText: {
    flexDirection: "row",
  },
});
