import * as WebBrowser from "expo-web-browser";
import {useEffect, useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { CurvedBottomBar } from 'react-native-curved-bottom-bar';
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';


import HeaderDataCard from '../screens/HeaderDataCard';
import CreateTweetDataCard from '../screens/CreateTweetDataCard';
import TweetPostDataCard from '../screens/TweetPostDataCard';

import addTweetImageUrl from "../assets/images/add_tweet_btn.png";
import { API_URL } from "../constants/api";


export default function Login({
  navigation,
}: RootTabScreenProps<"Home">) {

  var tmpTweetsList = [];
  if(localStorage["tweetsList"] != ""){
      tmpTweetsList = JSON.parse(localStorage["tweetsList"]);
  }

  const [followingListCount, setFollowingListCount]   = useState("0");
  const [followerListCount, setFollowerListCount]     = useState("0");
  const [tweetPostList, setTweetPostList]             = useState(tmpTweetsList);
  const [tweetsCount, setTweetsCount]                 = useState(localStorage["totalTweets"]);
  const [errorMsg, setErrorMsg] = useState("");
    
  var uuId = localStorage["uuId"];
  var name = localStorage["name"];
  var authToken = localStorage["token"];
  var email = localStorage["email"];
  var password = localStorage["password"];  
  var profileStatus = localStorage["profileStatus"];
  var active = localStorage["active"];

  function isUserLoggedIn(navigation){
        if(localStorage["email"] == ""){
            navigation.push("LoginTab");
        }
  }
  isUserLoggedIn(navigation);

  function forceLogoutUser(navigation) {
      localStorage.clear();
      navigation.push("LoginTab");
  }

  function showPostTweetPage(navigation) {    
      navigation.push("PostTweetTab");
  }

  function getFollowingList(navigation){

    var userUUID  = localStorage["uuId"];
    var authToken = localStorage["token"];
    // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/following";
    var postUrl   = `${API_URL}/users/${userUUID}/following`;
    var tmpData   = {"countFlag": true, "requestStatus": "Confirmed"};

    $.ajax({
          type: "GET",
          contentType: "application/json",
          url: postUrl,
          data: tmpData,
          beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", authToken);
          },
          success : function(response) {              
              if(response.status >= 200 && response.status <= 300){
                  setFollowingListCount(response.count);
              }
          },
          error : function(xhr) {
              setErrorMsg(xhr.responseJSON.message);
              if(xhr.responseJSON.message == "Invalid Token"){
                    forceLogoutUser(navigation);
              }
        }
      });
  }

  function getFollowerList(navigation){

    var userUUID  = localStorage["uuId"];
    var authToken = localStorage["token"];
    // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/followers";
    var postUrl   = `${API_URL}/users/${userUUID}/followers`
    var tmpData = {"countFlag": true, "requestStatus": "Confirmed"};

    $.ajax({
          type: "GET",
          contentType: "application/json",
          url: postUrl,
          data: tmpData,
          beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", authToken);
          },
          success : function(response) {              
              if(response.status >= 200 && response.status <= 300){
                setFollowerListCount(response.count);
              }
          },
          error : function(xhr) {
            setErrorMsg(xhr.responseJSON.message);
            if(xhr.responseJSON.message == "Invalid Token"){
                  forceLogoutUser(navigation);
            }
        }
      });
  }  

  function getTweetPostList(navigation){
    
      var userUUID  = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/tweets";
      var postUrl   = `${API_URL}/users/${userUUID}/tweets`
      var tmpData   = {"countFlag": false, "status": "Enabled", "tweetTab": "tweets"};

      $.ajax({
            type: "GET",
            contentType: "application/json",
            url: postUrl,
            data: tmpData,
            beforeSend: function (xhr) {
              xhr.setRequestHeader("x-access-token", authToken);
            },
            success : function(response) {
                if(response.status >= 200 && response.status <= 300){
                  var tweetPostListData = response.data;                  
                  setTweetPostList(tweetPostListData);
                  setTweetsCount(tweetPostListData.length);
                }              
            },
            error : function(xhr) {
              setErrorMsg(xhr.responseJSON.message);
              if(xhr.responseJSON.message == "Invalid Token"){
                    forceLogoutUser(navigation);
              }
            }
        });        
  }

  useEffect(() => {
      if(localStorage["uuId"] != "" && localStorage["token"] != ""){
            getFollowingList(navigation);
            getFollowerList(navigation);
            getTweetPostList(navigation);
      }
  }, []);


  
  
  //console.log("totalTweets -- " + localStorage["totalTweets"]);
  //console.log("tweetsCount -- " + tweetsCount);
  if(localStorage["uuId"] != "" && localStorage["token"] != "" && localStorage["totalTweets"] != tweetsCount){
        getTweetPostList(navigation);
        localStorage["totalTweets"] = tweetsCount;
  }
  

  return (
    <View style={styles.container}>
      
      <View style={styles.subContainer}>

          <View style={styles.headerContainer}>
              <HeaderDataCard key="headerTabData"  navigation={navigation} />
          </View>
    
          <Text style={styles.title}>Welcome {name }</Text>
          
          <View style={styles.textLinkContainer}>
              <TouchableOpacity  onPress={() => navigation.navigate("Following")} style={styles.linkText}>
                      <Text lightColor={Colors.light.tint}>{followingListCount} Following</Text>
              </TouchableOpacity>

              <TouchableOpacity  onPress={() => navigation.navigate("Follower")} style={styles.linkText}>      
                      <Text  style={{ marginLeft: '10px'}} lightColor={Colors.light.tint}>{followerListCount} Followers</Text>
              </TouchableOpacity>
          </View>

          <View style={styles.tweetContainer}>

              <Text style={{fontSize: 15, color: "rgb(47, 149, 220)", textAlign: "center", marginLeft: '88%' }}></Text>
              <TouchableOpacity onPress={() => showPostTweetPage(navigation)}>
                        <img src={addTweetImageUrl} style={{width: '43px', marginBottom: '10px'}} />
              </TouchableOpacity>              
          </View>

          <ScrollView style={styles.TweetPostListContainer}>
                { tweetPostList.length >= 1 ?  tweetPostList.map((tweetPostRow, index) => <TweetPostDataCard key={index}  navigationData={navigation}  tweetPostRow={tweetPostRow} />) : <Text style={styles.normalText}>You haven't any tweet yet</Text> }
          </ScrollView>      
     </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '15px',
  },
  tweetContainer: {
    flexDirection: "row",
    marginTop: '15px',
  },
  subContainer: {
    marginLeft: '34px',
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '15px',
  },
  TweetPostListContainer: {
    flexDirection: "column",    
    marginTop: '20px',
    maxHeight: '430px',
    marginBottom: "10px"
  },
  PageTitle: {
    fontSize: 17,
    fontWeight: "bold",
    margin: "15px",
    textAlign: "left",
    width: "75%",    
  },  
  midBodyContainer: {
    flexDirection: "row",
    marginTop: '15px',
  },
  textLinkContainer: {
    flexDirection: "row",
  },
  menuLink: {
    flexDirection: "row",
  },
  textLink: {
    flexDirection: "row"
  },
  linkText: {
    flexDirection: "row"
  },
  title: {
    fontSize: 17,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: '3px',
    width: "70%",
    
  },
  normalText: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    marginTop: '3px',
    width: "70%",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "left"
  },
  menuLinkText: {
    textAlign: "left",
    marginLeft: '16px',
    marginTop: '7px',
  },
  logoutLinkText: {
    textAlign: "left",
    marginTop: '7px',
  },  
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },  
});
