import * as WebBrowser from "expo-web-browser";
import {useEffect, useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import FollowerDataCard from '../screens/FollowerDataCard';
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import HeaderDataCard from '../screens/HeaderDataCard';
import { API_URL } from "../constants/api";

export default function FollowerList({navigation}) {

  const [errorMsg, setErrorMsg] = useState("");
  const [followerList, setFollowerList] = useState([]);


  function isUserLoggedIn(navigation){
      if(localStorage["email"] == ""){
          navigation.push("LoginTab");
      }
  }
  isUserLoggedIn(navigation);
  
  var uuId = localStorage["uuId"];
  var name = localStorage["name"];
  var authToken = localStorage["token"];
  var email = localStorage["email"];
  var password = localStorage["password"];  
  var profileStatus = localStorage["profileStatus"];
  var active = localStorage["active"];

  
  function getFollowerList(){

    var userUUID  = localStorage["uuId"];
    var authToken = localStorage["token"];
    var postUrl   = `${API_URL}/users/${userUUID}/followers`;

    var tmpData = {"countFlag": false, "requestStatus": "Confirmed"};

    $.ajax({
          type: "GET",
          contentType: "application/json",
          url: postUrl,
          data: tmpData,
          beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", authToken);
          },
          success : function(response) {              
              if(response.status >= 200 && response.status <= 300){
                  setFollowerList(response.data);
              }
          },
          error : function(xhr) {            
            console.log(xhr.responseJSON.message);
            setErrorMsg(xhr.responseJSON.message);
        }
      });
  }
  
  

 useEffect(() => {
  console.log("this is folllowers  list")
  getFollowerList();
}, []);

  

  return (
    <View style={styles.mainContainer}>

          <View style={styles.headerContainer}>
              <HeaderDataCard key="headerTabData"  navigation={navigation} />
          </View>  

          <View style={styles.followerListContainer}>
              { followerList.length >= 1 ?  followerList.map((fwList, index) => <FollowerDataCard key={index}  fwList={fwList} />) : <Text style={styles.title}>You don't have any followers yet</Text> }
          </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  followerListContainer: {
    flexDirection: "column",
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },
  menuLink: {
    flexDirection: "row",
  },
  PageTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: "34px",
    textAlign: "left",
    width: "75%",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "left",
    marginLeft: "34px",
    marginTop: '3px',
    width: "70%",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  logoutLinkText: {
    textAlign: "left",
    marginTop: '7px',
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 100,
    height: "40px",
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    borderRadius: 10,
    height: "40px",
    display: "flex",
  },
});
