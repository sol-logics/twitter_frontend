// import React from 'react'
// import { Alert, Text, TouchableOpacity, StyleSheet } from 'react-native'

// const AlertExample = () => {
//    const showAlert = () =>{
//       Alert.alert(
//          'You need to...'
//       )
//    }
//    return (
//       <TouchableOpacity onPress = {showAlert} style = {styles.button}>
//          <Text>Alert</Text>
//       </TouchableOpacity>
//    )
// }
// export default AlertExample

// const styles = StyleSheet.create ({
//    button: {
//       backgroundColor: '#4ba37b',
//       width: 100,
//       borderRadius: 50,
//       alignItems: 'center',
//       marginTop: 100
//    }
// })

import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View } from "react-native";

const AlertExample = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.titleText}>{props.title}</Text>
            <Text style={styles.modalText}>Are you sure {props.desc}</Text>
            <View style={styles.actionView}>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Yes </Text>
              </Pressable>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>No </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Show Modal</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,

    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    width: "90%",
    alignItems: "center",
    shadowColor: "#000",
    
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
   

  },
  actionView: {
   // width:"100px",
   display:"flex",
   flexDirection:"row",
   justifyContent:"space-between"
 },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
    width:"150px"
  },
  buttonClose: {
    backgroundColor: "#2196F3",
    width:"150px"
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  titleText: {
   marginBottom: 15,
   fontSize:"18px",
   textStyle:"bold",
   textAlign: "center",
 },
});

export default AlertExample;
