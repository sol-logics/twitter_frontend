import * as WebBrowser from "expo-web-browser";
import { useState, useEffect } from "react";
import {
  TextInput,
  Platform,
  Button,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import DatePicker from "react-native-date-picker";
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import DateTimePicker from "@react-native-community/datetimepicker";
import Moment from "moment";

import moment from "moment";
// import DatePicker from 'react-native-date-picker'
import * as $ from "jquery";
// import DateTimePicker from '@react-native-community/datetimepicker';
import HeaderDataCard from "../screens/HeaderDataCard";
import { BottomTabNavigator } from "../navigation";
import { useDeepCompareEffect } from "use-deep-compare-effect";
import { getUserdata } from "../constants/restapi";
// import { getUserdata } from "../constants/restapi";
import { type } from "./../types";
import { API_URL } from "../constants/api";
export default function UserProfile({ navigation }) {
  var uuId = localStorage["uuId"];
  const [mydate, setDate] = useState(new Date());
  const [displaymode, setMode] = useState("date");
  var name = localStorage["name"];
  const [date, set1Date] = useState(new Date());
  var authToken = localStorage["token"];
  // const [date, setDate] = useState("09-10-2020");

  var email = localStorage["email"];
  const [errorMsg, setErrorMsg] = useState("");
  // const [date, setDate] = useState(new Date())
  const [open, setOpen] = useState(false);
  const [isEditable, setisEditable] = useState(false);
  const [profileData, setProfileData] = useState({
    dob: new Date(),
    email: null,
    firstName: null,
    uuId: null,
    lastName: null,
    middleName: null,
    phoneNumber: null,
  });
  var password = localStorage["password"];
  var profileStatus = localStorage["profileStatus"];
  var active = localStorage["active"];

  function isUserLoggedIn(navigation) {
    if (localStorage["email"] == "") {
      navigation.push("LoginTab");
    }
  }

  function onEditableChange() {
    setisEditable(true);
  }
  const [isDisplayDate, setShow] = useState(false);
  const changeSelectedDate = (event, selectedDate) => {
    const currentDate = selectedDate || mydate;
    setDate(currentDate);
  };
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };
  const displayDatepicker = () => {
    showMode("date");
  };
  const onInputChange = (key, value) => {
    console.log(key, value);
    // if (key === "email") {
    //   userData.refUser.email = value;
    // } else if (key === "active") {
    //   userData.refUser.active = value;
    // } else if (key === "mobile") {
    //   userData.refUser.mobile = value;
    // } else {
    //   userData[key] = value;
    // }
    profileData[key] = value;
    setProfileData({ ...profileData });
    console.log(profileData);
    // validateField(value, key);
  };

  useEffect(() => {
    getProfileData();
    // getUserdata()
    //   .then(function (res) {
    //     console.log(res);
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });
  }, []);

  isUserLoggedIn(navigation);

  function getProfileData() {
    var userUUID = localStorage["uuId"];
    var authToken = localStorage["token"];
    // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/profiledetail";
    var postUrl = `${API_URL}/users/${userUUID}/profiledetail`;
    // var tmpData = {"countFlag": false, "requestStatus": "Confirmed"};

    $.ajax({
      type: "GET",
      contentType: "application/json",
      url: postUrl,
      // data: tmpData,
      beforeSend: function (xhr) {
        xhr.setRequestHeader("x-access-token", authToken);
      },
      success: function (response) {
        if (response.status >= 200 && response.status <= 300) {
          console.log(response.data);
          // ...{dob:Moment(new Date()).format('DD-MM-YYYY')}
          setProfileData({
            ...profileData,
            ...response.data[0],
            ...{ dob: null },
          });
          console.log(profileData);
        }
      },
      error: function (xhr) {
        console.log(xhr.responseJSON.message);
        setErrorMsg(xhr.responseJSON.message);
      },
    });
  }

  function setProfile() {
    var emailId = localStorage["userEmailForForgotPass"];
    try {
      var userUUID = localStorage["uuId"];
      var authToken = localStorage["token"];
      // var postUrl = "http://localhost:8080/api/users/" + userUUID + "/profile";
      var postUrl = `${API_URL}/users/${userUUID}/profile`;
      var tmpData = profileData;
      //console.log("tmpData -- " + JSON.stringify(tmpData));
      var response = null;
      $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: postUrl,
        data: JSON.stringify(tmpData),
        dataType: "json",
        beforeSend: function (xhr) {
          xhr.setRequestHeader("x-access-token", authToken);
        },
        success: function (result) {
          response = result;
          console.log("response -- " + JSON.stringify(response));
          if (response.status >= 200 && response.status <= 300) {
            setErrorMsg("");
            navigation.push("Home");
          } else {
            setErrorMsg(response.message);
          }
        },
        error: function (xhr) {
          console.log(xhr.responseJSON.message);
          setErrorMsg(xhr.responseJSON.message);
        },
      });
    } catch (e) {
      alert(e);
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <HeaderDataCard key="headerTabData" navigation={navigation} />
      </View>

      <View style={styles.formContainer}>
        <Text style={styles.PageTitle}>Profile Page</Text>
        <TextInput
          name="First Name"
          editable={isEditable}
          style={styles.textInputField}
          placeholder="First Name"
          value={profileData.firstName}
          onChange={(e) => onInputChange("firstName", e.target.value)}
        />
        <TextInput
          name="Middle Name"
          editable={isEditable}
          style={styles.textInputField}
          placeholder="Middle Name"
          value={profileData.middleName}
          onChange={(e) => onInputChange("middleName", e.target.value)}
        />
        <TextInput
          name="Last Name"
          editable={isEditable}
          style={styles.textInputField}
          placeholder="Last Name"
          value={profileData.lastName}
          onChange={(e) => onInputChange("lastName", e.target.value)}
        />

        <TextInput
          name="Email"
          type={email}
          editable={false}
          style={styles.textInputField}
          placeholder="email"
          value={profileData.email}
          onChange={(e) => onInputChange("email", e.target.value)}
        />
        <TextInput
          name="Mobile No"
          type="tel-country-code"
          maxLength={10}
          editable={isEditable}
          style={styles.textInputField}
          placeholder="Mobile No"
          value={profileData.phoneNumber}
          onChange={(e) => onInputChange("phoneNumber", e.target.value)}
        />

        {/* <TextInput
          name="Date of Birth"
          editable={isEditable}
          style={styles.textInputField}
          placeholder="Date of Birth"
          value={profileData.dob}
          onChange={(e) => onInputChange("dob", e.target.value)}
        /> */}
      </View>

      <View style={styles.buttonContainer}>
        {isEditable ? (
          <TouchableOpacity
            // onPress={() => signInUser(navigation, email, password)}
            onPress={() => setProfile()}
            style={styles.signInActionButton}
          >
            <Text style={{ fontSize: 16, color: "#fff", textAlign: "center" }}>
              Save
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            // onPress={() => signInUser(navigation, email, password)}
            onPress={() => onEditableChange()}
            style={styles.signInActionButton}
          >
            <Text style={{ fontSize: 16, color: "#fff", textAlign: "center" }}>
              Edit
            </Text>
          </TouchableOpacity>
        )}
      </View>

      {/* <View ><BottomTabNavigator /></View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: "20px",
    marginLeft: "34px",
  },
  buttonContainer: {
    marginTop: "10px",
    alignItems: "center",
  },
  datePickerStyle: {
    width: 200,
    marginTop: 20,
  },
  formContainer: {
    alignItems: "center",
    textAlign: "center",
    marginLeft: "10px",
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: "20px",
  },
  menuLink: {
    flexDirection: "row",
  },
  PageTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: "34px",
    textAlign: "left",
    width: "75%",
  },
  title: {
    fontSize: 18,
    fontWeight: "normal",
    textAlign: "left",
    marginLeft: "34px",
    marginTop: "3px",
    width: "70%",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "81%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  logoutLinkText: {
    textAlign: "left",
    marginTop: "7px",
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  signInActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    marginRight: "20px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "200px",
    paddingTop: "5px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
});
