import * as WebBrowser from "expo-web-browser";
import {useEffect, useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";
import Moment from 'moment';
import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';
import userImageUrl from "../assets/images/user.jpg";
import likeImageUrl from "../assets/images/likes.png";
import likedImageUrl from "../assets/images/liked.png";
import replyToImageUrl from "../assets/images/twitter_reply.png";
import { API_URL } from './../constants/api';

export default function TweetCommentDataCard(props) {

Moment.locale('en');

var navigation = props.navigationData;
var commentDate = props.tweetCommentRow.createdDate;

//var totalComments = props.tweetCommentRow.countComment;
//var totalLikes = props.tweetCommentRow.countLike;
//var likeFlag = props.tweetCommentRow.likeFlag;

var totalComments = 2;
var totalLikes = 1;
var likeFlag = "false";

var userLikeImageUrl = likeImageUrl;
  if(likeFlag == "true"){
     userLikeImageUrl = likedImageUrl;
  }

const [errorMsg, setErrorMsg] = useState("");

function showTweetCommentReplyPage(navigation, tweetId, commentId) {    
      navigation.push("ReplyCommentTab",{tweetId, commentId});
}

function likeCommentPost(tweetId, commentId){
  try{       
    var userUUID  = localStorage["uuId"];
    var authToken = localStorage["token"];    
    // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/tweets/"+tweetId+"/comments/"+commentId+"/like";
    var postUrl   = `${API_URL}/users/${userUUID}/tweets/${tweetId}/comments/${commentId}/like`;

    var response = null;
    $.ajax({
          type: "POST",
          url: postUrl,
          beforeSend: function (xhr) {
              xhr.setRequestHeader("x-access-token", authToken);
          },
          success : function(result) {
              response = result;                
              if(response.status >= 200 && response.status <= 300){
              }
          },
          error : function(xhr) {
              setErrorMsg(xhr.responseJSON.message);
          }
      });				
    }catch(e){
        alert(e);
    }
}
  
 return (
    <View style={styles.container}>
      <View style={styles.subLeftContainer}>
        <img src={userImageUrl} style={{ width: '43px', height: '43px'}} alt={props.tweetCommentRow.commentId} />
      </View>

      <View style={styles.subMiddleContainer}>
          <Text style={styles.tweetContent}>{props.tweetCommentRow.comment}</Text>
          <Text style={styles.tweetContent}>{Moment(commentDate).format('d MMM YYYY')}</Text>


          <View style={styles.replyNLikeContainer}>
          <TouchableOpacity onPress={() => showTweetCommentReplyPage(navigation, props.tweetCommentRow.tweetId, props.tweetCommentRow.commentId)} style={styles.linkText} >
            <img src={replyToImageUrl} style={{ marginTop: "3px", width: "14px", height: "14px", float: "left"}} alt={props.tweetCommentRow.commentId} />
            <Text lightColor={Colors.light.tint}>{" "}{totalComments}</Text>
          </TouchableOpacity>

          <TouchableOpacity  onPress={() => likeCommentPost(props.tweetCommentRow.tweetId, props.tweetCommentRow.commentId)} style={styles.linkText}>
                  <img src={likeImageUrl} style={{marginTop: '3px', width: '14px', height: '14px', float: 'left'}} alt={props.tweetCommentRow.commentId} />
                  <Text lightColor={Colors.light.tint}> {totalLikes}</Text>
          </TouchableOpacity>
        </View>
        
      </View>

      
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    minHeight: 'fit-content',
  },
  subLeftContainer: {
    flexDirection: "column",
    marginTop: '10px',
    width: "8%",
  },
  subMiddleContainer: { 
    flexDirection: "column",
    marginLeft: '10px',
    marginTop: '20px',
    width: "80%",    
  },
  replyNLikeContainer: {
    flexDirection: "row",
    marginBottom: "5px",
  },
  tweetContent: {
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",
  },  
  linkText: {
    flexDirection: "row",
    maxWidth: "70px",
    flex: 1,
  },
});
