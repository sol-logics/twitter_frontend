import * as WebBrowser from "expo-web-browser";
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import UserDataCard from '../screens/UserDataCard';
import * as $ from 'jquery';

import HeaderDataCard from '../screens/HeaderDataCard';
import { API_URL } from "../constants/api";

export default function Login({
  navigation,
}: RootTabScreenProps<"Home">) {
  
  var uuId = localStorage["uuId"];
  var name = localStorage["name"];
  var authToken = localStorage["token"];
  var email = localStorage["email"];
  var password = localStorage["password"];  
  var profileStatus = localStorage["profileStatus"];
  var active = localStorage["active"];

  const [search, setSearch] = useState("");
  const [userList, setUserList] = useState([]);
  const [errorMsg, setErrorMsg] = useState("");
  const [noUserFound, setNoUserFound] = useState("");

  function isUserLoggedIn(navigation){
      if(localStorage["email"] == ""){
          navigation.push("LoginTab");
      }
  }
  isUserLoggedIn(navigation);  

  function getSearchedUserList(search){
        var userUUID  = localStorage["uuId"];
        var authToken = localStorage["token"];       
        console.log("authToken - " + authToken); 
        var postUrl   = `${API_URL}/users/${userUUID}/usersList`;
        // var postUrl   = "http://localhost:8080/api/users/"+userUUID+"/usersList";
        var tmpData   = {"countFlag": false, "searchUserString": search};

        $.ajax({
              type: "GET",         
              contentType: "application/json",    
              url: postUrl,
              data: tmpData,
              dataType: "json",
              beforeSend: function (xhr) {
                xhr.setRequestHeader("x-access-token", authToken);
              },
              success : function(response) {                  
                  if(response.status >= 200 && response.status <= 300){
                      var userListData = response.data;
                      setUserList(userListData);
                      if(userListData.length == 0){
                          setNoUserFound("No user found");
                      }
                  }
              },
              error : function(xhr) {
                  //console.log(xhr.responseJSON.message);
                  console.log("responseJSON -- " + JSON.stringify(xhr));
                  setErrorMsg(xhr.responseJSON.message);
              }
          });
    }


  return (
    <View style={styles.container}>

          <View style={styles.headerContainer}>
              <HeaderDataCard key="headerTabData"  navigation={navigation} />
          </View>

      <Text style={styles.PageTitle}>Search for people</Text>

      <View style={styles.searchContainer}>
          <TextInput name="search" style={styles.textInputField} placeholder="Search" value={search} onChange={(e) => setSearch(e.target.value)} />

          <TouchableOpacity
          disabled ={search==""}
          onPress={() => getSearchedUserList(search)} style={styles.actionButton} >
              <Text style={{ fontSize: 17, color: "#fff", textAlign: "center" }}>Search</Text>
            </TouchableOpacity>
        </View>

      <View style={styles.userListContainer}>
              { userList.length >= 1 ?  userList.map((userRow, index) => <UserDataCard key={index}  userRow={userRow} />) : <Text style={styles.noUserMsg}>{noUserFound}</Text> }
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
  },
  headerContainer: {
    flexDirection: "row",
    marginTop: '20px',
    marginLeft: '34px',
  },
  menuContainer: {
    flexDirection: "row",
    marginTop: '20px',
  },
  userListContainer: {
    flexDirection: "column",
  },
  menuLink: {
    flexDirection: "row",
  },
  searchContainer: {
    flexDirection: "row",
  },
  PageTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: "34px",
    textAlign: "left",
    width: "75%",
  },
  title: {
    fontSize: 18,
    fontWeight: "normal",
    textAlign: "left",
    marginLeft: "34px",
    marginTop: '3px',
    width: "70%",
  },
  noUserMsg: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    marginLeft: "34px",
    marginTop: '20px',
    width: "70%",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  textInputField: {
    width: "61%",
    padding: "10px",
    textAlign: "left",
    marginTop: "10px",
    marginLeft: "34px",
    borderStyle: "solid",
    borderColor: "#ccc",
    borderWidth: 1,
  },
  helpContainer: {
    alignItems: "center",
    marginTop: "160px",
  },
  actionButtonContainer: {
    flexDirection: "row",
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: "center",
  },
  logoutLinkText: {
    textAlign: "left",
    marginTop: '7px',
  },
  signUpContainer: {
    flexDirection: "row",
  },
  signUpLinkText: {
    textAlign: "center",
    fontSize: 15,
  },
  actionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "7px",
    marginTop: "10px",
    marginLeft: "20px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
  signUpActionButton: {
    backgroundColor: "rgb(29, 155, 240)",
    width: "151px",
    paddingTop: "7px",
    borderRadius: 9999,
    height: "40px",
    display: "flex",
  },
});
