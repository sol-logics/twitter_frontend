import * as WebBrowser from "expo-web-browser";
import {useState } from "react";
import { TextInput, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/Colors";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import * as $ from 'jquery';

import logoutImageUrl from "../assets/images/logout.jpg";
import userImageUrl from "../assets/images/user.jpg";

export default function FollowingCard(props) {
  
 return (
    <View style={styles.container}>
      <View style={styles.subLeftContainer}>
        <img src={userImageUrl} style={{ width: '43px', height: '43px'}} alt={props.fwList.name} />
      </View>

      <View style={styles.subMiddleContainer}>
        <Text style={styles.title}>{props.fwList.firstName} {props.fwList.lastName} </Text>
        <Text style={styles.title}>{props.fwList.email}</Text>
      </View>

      <View style={styles.subRightContainer}>
      <TouchableOpacity  onPress={() => navigation.navigate("FollowerListTab")}style={styles.linkText}>        
          <Text  style={{ marginLeft: '10px'}} lightColor={Colors.light.tint}>Following</Text>
      </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "left",
    flexDirection: "row",
  },
  subLeftContainer: {
    flexDirection: "row",
    marginLeft: '34px',
    marginTop: '20px',
    width: "8%",
    maxHeight: '50px !important',
  },
  subMiddleContainer: { 
    flexDirection: "column",
    marginLeft: '20px',
    marginTop: '20px',
    width: "50%",
    maxHeight: '50px !important',
  },
  subRightContainer: {
    flexDirection: "column",
    marginLeft: '10px',
    marginTop: '20px',
    width: "40%",
    maxHeight: '50px !important',
  },
  title: {
    fontSize: 15,
    fontWeight: "normal",
    textAlign: "left",
    width: "98%",    
  },
  linkText: {
    flexDirection: "row"
  },
});
